
import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\texport GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']



async def query():
    """fetches token and data."""
    token = await get_token()
    print(token)


if __name__ == "__main__":
    asyncio.run(query())
