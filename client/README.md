# Oly Register Client

Client interacting with the Oly Register API

## Setup

### Install dependencies

```console
pip install -r requirements.txt
```

### Configure the credentials

The scripts are using the following environment variables for the authentication

```console
export GRAPHQLUSER="admin user"
export GRAPHQLPASSWORD="admin password"
```

## Avaibable scripts

### Create all delegations

Create all delegations with default password.  
WARNING: This script could reset the delegations. Run it only once in the beginning of the event.

```console
// see WARNING above
// create an excel file delegations.xlsx
python create_delegations.py
```

### Download registrations

Download the student and leader details.

```console
python download_registrations.py
```
