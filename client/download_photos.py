from datetime import datetime
from pathlib import Path
# import pandas as pd
import typer

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
import requests

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


QUERY_REGISTRATIONS = gql("""
    query {
        allDelegations {
            name
            leaderSet {
                code
                photo
            }
            studentSet {
                code
                photo
            }
        }
    }
""")


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\t<export GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def fetch_data(token, query):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        resp = await session.execute(query)
        return resp
        

async def run(output_dir: Path):
    now = datetime.now()

    typer.secho("Authenticating...", fg=typer.colors.BLUE)
    token = await get_token()


    typer.secho("Fetch data...")
    resp = await fetch_data(token, QUERY_REGISTRATIONS)
    for delegation in resp["allDelegations"]:
        for item in (*delegation["studentSet"], *delegation["leaderSet"]):
            output_file: Path = output_dir / (item["code"] + ".png")
            typer.secho(f"Dowloading photo to {output_file}", fg=typer.colors.BLUE)
            r = requests.get(item["photo"])
            try:
                r.raise_for_status()
                output_file.write_bytes(r.content)
                typer.secho("   ok", fg=typer.colors.GREEN)
            except requests.exceptions.HTTPError as err:
                if err.response.status_code == 404:
                    typer.secho("   not found", fg=typer.colors.RED)
                else:
                    raise


    typer.secho("Finished!", fg=typer.colors.GREEN)


def main(output_dir: Path):
    output_dir.mkdir(exist_ok=True)
    asyncio.run(run(output_dir))


if __name__ == "__main__":
    typer.run(main)

