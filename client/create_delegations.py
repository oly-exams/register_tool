import pandas as pd

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\texport GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def create_delegation(token, code, country, email, password):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    # we need to use double curly brackets in f-templates.
    query = gql(f"""
        mutation {{
            createDelegation(code: "{code}", country: "{country}", email: "{email}", password: "{password}") {{
                success
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        await session.execute(query)
        

async def main():

    df = pd.read_excel("delegations.ods", sheet_name="leader1")
    print(df)


    token = await get_token()

    for i, row in df.iterrows():
        print(row["Code"])
        password = row["password"].replace("\"", "_").replace("'", "_")
        await create_delegation(token, row["Code"], row["Country"], row["Email"], password)



if __name__ == "__main__":
    asyncio.run(main())



