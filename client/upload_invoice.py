from datetime import datetime
from pathlib import Path
import re
import typer

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\texport GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def upload_invoice(token, code, document_id, filename):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    # we need to use double curly brackets in f-templates.
    query = gql(f"""
        mutation ($file: Upload!) {{
            uploadDelegationdocument(delegationCode: "{code}", documentType: "{document_id}", document: $file) {{
                success
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:
        with open(filename, "rb") as f:
            params = {"file": f}
            await session.execute(query, variable_values=params, upload_files=True)
        

async def run(document_type_id: int, invoice_dir: Path):
    typer.echo(f"Uploading from {invoice_dir}")

    token = await get_token()

    now = datetime.utcnow()
    pattern = re.compile(r".*IPhO2022_Invoice_([A-Z]{3})\.pdf")

    for fname in invoice_dir.glob("*.pdf"):

        match = pattern.match(str(fname))
        if not match:
            continue
        code = match.group(1)

        typer.secho(f"Uploading {fname} for delegation {code}..", fg=typer.colors.BLUE)
        success = True

        try:
            await upload_invoice(token, code, document_type_id, fname)
        except Exception as err:
            success = False
            typer.secho(err, fg=typer.colors.RED)
        
        if success:
            typer.secho("Success", fg=typer.colors.GREEN)


def main(document_type_id: int, invoice_dir: Path):

    asyncio.run(run(document_type_id, invoice_dir))


if __name__ == "__main__":
    typer.run(main)



