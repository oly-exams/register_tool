import os

filedir = os.path.dirname(os.path.realpath(__file__))

# graphql API
GRAPHQL = "https://api.ipho22-registration.oly-exams.org/graphql"

# username for graphql API
USER = os.getenv("GRAPHQLUSER", "admin")
PASSWORD = os.getenv("GRAPHQLPASSWORD")


