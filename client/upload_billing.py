from datetime import datetime
from pathlib import Path
import pandas as pd
import typer

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\texport GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def add_billing_line(token, code, date,  description, amount):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    # we need to use double curly brackets in f-templates.
    query = gql(f"""
        mutation {{
            addTransaction(code: "{code}", date: "{date.isoformat()}", description: "{description}", amount: "{amount}") {{
                success
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        await session.execute(query)
        

async def run(billing_df: pd.DataFrame):

    token = await get_token()

    now = datetime.utcnow()

    for i, row in billing_df.iterrows():
        typer.secho(f"Adding transactions for {row['code']}..", fg=typer.colors.BLUE)
        success = True

        try:
            cost = row['cost_students']
            num = row['n_students']
            text = f"Registration fee for {num} Student(s)"
            await add_billing_line(token, row['code'], now, text, cost)
        except Exception as err:
            success = False
            typer.secho(err, fg=typer.colors.RED)

        try:
            cost = row['cost_leaders']
            num = row['n_leaders']
            text = f"Registration fee for {num} Leader(s)"
            await add_billing_line(token, row['code'], now, text, cost)
        except Exception as err:
            success = False
            typer.secho(err, fg=typer.colors.RED)

        try:
            cost = row['cost_observers']
            num = row['n_observers']
            text = f"Registration fee for {num} Observer(s)"
            await add_billing_line(token, row['code'], now, text, cost)
        except Exception as err:
            success = False
            typer.secho(err, fg=typer.colors.RED)

        try:
            cost = row['cost_inviligators']
            num = row['n_inviligators']
            text = f"Registration fee for {num} Inviligator(s)"
            await add_billing_line(token, row['code'], now, text, cost)
        except Exception as err:
            success = False
            typer.secho(err, fg=typer.colors.RED)
        
        if success:
            typer.secho("Success", fg=typer.colors.GREEN)


def main(billing_file: Path):
    typer.echo(f"Loading {billing_file}")

    df = pd.read_excel(billing_file)
    print(df)

    asyncio.run(run(df))


if __name__ == "__main__":
    typer.run(main)



