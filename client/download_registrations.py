from datetime import datetime
import pandas as pd

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


QUERY_LEADERS = gql("""
    query {
        allDelegations {
            name
            leaderSet {
                code
                submitted
                givenName
                lastName
                namePrefix
                gender
                birthday
                email
                emailAuthorization
                residenceCountry
                residenceAddress
                experimentAddress
                tshirtSize
                medicalIssue
            }
        }
    }
""")

QUERY_STUDENTS = gql("""
    query {
        allDelegations {
            name
            studentSet {
                code
                givenName
                lastName
                certificateName
                gender
                birthday
                email
                emailAuthorization
                schoolName
                schoolAddress
                tshirtSize
                medicalIssue
            }
        }
    }
""")


async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\t<export GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def fetch_data(token, query):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        resp = await session.execute(query)
        return resp
        

async def main():
    now = datetime.now()

    logging.info("Authenticating...")
    token = await get_token()


    logging.info("Fetch leaders...")
    resp = await fetch_data(token, QUERY_LEADERS)
    df = pd.json_normalize(resp['allDelegations'], record_path='leaderSet', meta='name', meta_prefix='delegation_')
    output_filename=f"leaders_{now.strftime('%Y%m%d-%H%M%S')}.xlsx"
    df.to_excel(output_filename)
    logging.info("Results written to %s", output_filename)


    logging.info("Fetch students...")
    resp = await fetch_data(token, QUERY_STUDENTS)
    df = pd.json_normalize(resp['allDelegations'], record_path='studentSet', meta='name', meta_prefix='delegation_')
    output_filename=f"students_{now.strftime('%Y%m%d-%H%M%S')}.xlsx"
    df.to_excel(output_filename)
    logging.info("Results written to %s", output_filename)


if __name__ == "__main__":
    asyncio.run(main())



