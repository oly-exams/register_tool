from datetime import datetime
import pandas as pd

import asyncio
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

from constants import GRAPHQL, PASSWORD, USER

import logging
# logging.basicConfig(level=logging.DEBUG)


QUERY_DELEGATIONS = gql("""
    query {
        allDelegations {
            name
            country
            email
            address
            studentSet {
                code
                submitted
                givenName
                lastName
            }
            leaderSet {
                code
                type
                submitted
                givenName
                lastName
            }
        }
    }
""")



async def get_token():
    transport = AIOHTTPTransport(url=GRAPHQL, )
    # client = Client(transport=transport)

    if PASSWORD is None:
        print("You did not set password. Enter")
        print("\t<export GRAPHQLPASSWORD=yourpassword")
        exit(1)

    # we need to use double curly brackets in f-templates.
    mutation = gql(f"""
        mutation {{
            tokenAuth (username: "{USER}", password: "{PASSWORD}") {{
                token
            }}
        }}
    """)
    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        result = await session.execute(mutation)
        return result['tokenAuth']['token']


async def fetch_data(token, query):
    transport = AIOHTTPTransport(url=GRAPHQL, headers={"Authorization": f"Bearer {token}"})

    async with Client(
        transport=transport,
        execute_timeout=90
        # fetch_schema_from_transport=True,
    ) as session:

        resp = await session.execute(query)
        return resp
        

async def main():
    now = datetime.now()

    logging.info("Authenticating...")
    token = await get_token()


    logging.info("Fetch data...")
    resp = await fetch_data(token, QUERY_DELEGATIONS)

    data = []
    for dg in resp['allDelegations']:
        row = {
            'code': dg['name'],
            'country': dg['country'],
            'email': dg['email'],
            'address': dg['address'],
            'code': dg['name'],
            'n_students': len([ii for ii in dg['studentSet'] if ii['code'] != ""]),
            'n_leaders': len([ii for ii in dg['leaderSet'] if ii['code'] != "" and ii['type'] == "L"]),
            'n_observers': len([ii for ii in dg['leaderSet'] if ii['code'] != "" and ii['type'] == "O"]),
            'n_inviligators': len([ii for ii in dg['leaderSet'] if ii['code'] != "" and ii['type'] == "I"]),
        }
        row.update({
            'cost_students': 200 * row['n_students'],
            'cost_leaders': 200 * row['n_leaders'],
            'cost_observers': 200 * row['n_observers'],
            'cost_inviligators': 0 * row['n_inviligators'],
        })
        row['total'] = row['cost_students'] + row['cost_leaders'] + row['cost_observers'] + row['cost_inviligators']

        data.append(row)
    

    df = pd.json_normalize(data)
    print(df)

    output_filename=f"billing_{now.strftime('%Y%m%d-%H%M%S')}.xlsx"
    df.to_excel(output_filename)
    logging.info("Results written to %s", output_filename)


if __name__ == "__main__":
    asyncio.run(main())



