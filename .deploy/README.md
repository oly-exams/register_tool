# Deployment documentation

## 1. Install k3s

From https://k3s.io:

```console
curl -sfL https://get.k3s.io | sh -
# Check for Ready node,
takes maybe 30 seconds
k3s kubectl get node
```

Obtain KUBECONFIG file...

## 2. Volume for local storage

Data will be stored using the k3s local-storage provider.
By default the volumes will be created in `/var/lib/rancher/k3s/storage`.


## 2. Setup cluster via Ansible

...

