#!/bin/bash

REGISTRY=registry.gitlab.com/oly-exams/register_tool
TAG=dev

set +ex

docker build --target frontend-production -t ${REGISTRY}/oly_register-frontend:${TAG} .
docker build --target backend-production -t ${REGISTRY}/oly_register-backend:${TAG} .

docker push ${REGISTRY}/oly_register-frontend:${TAG}
docker push ${REGISTRY}/oly_register-backend:${TAG}
