import { defineStore } from "pinia";
import {
  provideApolloClient,
  useMutation,
  useQuery,
} from "@vue/apollo-composable";
import { apolloClient } from "@/apollo";
import { LOGIN_USER, REFRESH_TOKEN } from "@/apollo/mutations";
import { LOGGED_IN_USER } from "@/apollo/queries";
import router from "@/router";

provideApolloClient(apolloClient);

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    token: "",
    isAuthenticated: false,
    loading: false,
    username: "",
    delegation: {
      name: "",
      country: "",
    },
  }),
  getters: {
    // doubleCount: (state) => state.counter * 2,
  },
  actions: {
    async loginUser(username: string, password: string) {
      console.log("Authenticating with username and password.");

      try {
        // using the direct apolloClient here such that we can await for it
        this.loading = true;
        const { data } = await apolloClient.mutate({
          mutation: LOGIN_USER,
          variables: { username: username, password: password },
        });

        if (data && data.tokenAuth) {
          this.token = data.tokenAuth.token;
          this.isAuthenticated = true;
          console.log("auth done, now calling getUserDetails...");
          await this.getUserDetails();
        }
      } catch (err) {
        console.error("💣 There was an error 💣", err);
        this.$reset();
      } finally {
        this.loading = false;
      }
    },

    logout() {
      this.$reset();
      router.push({ name: "login" });
    },

    async getUserDetails() {
      console.log("Calling LOGGED_IN_USER");
      try {
        const { data } = await apolloClient.query({ query: LOGGED_IN_USER });
        if (data && data.me) {
          this.username = data.me.username;
          this.isAuthenticated = true;
          if (data.me.delegationSet) {
            this.delegation.name = data.me.delegationSet[0].name;
            this.delegation.country = data.me.delegationSet[0].country;
          }
        }
      } catch (err) {
        console.error("💣 There was an error 💣", err);
        this.$reset();
      }
    },

    async validateRefreshToken(newToken: string) {
      try {
        // using the direct apolloClient here such that we can await for it
        this.loading = true;
        const { data } = await apolloClient.mutate({
          mutation: REFRESH_TOKEN,
          variables: { token: newToken },
        });

        if (data && data.refreshToken) {
          this.token = data.refreshToken.token;
          this.isAuthenticated = true;
          console.log("auth done, now calling getUserDetails...");
          await this.getUserDetails();
        }
      } catch (err) {
        console.error("💣 There was an error 💣", err);
        this.$reset();
      } finally {
        this.loading = false;
      }
    },
  },
});
