// import gql from "graphql-tag";
import { gql } from "@app/gql";

export const DELEGATION_CONTACT = gql(/* GraphQL */ `
  query getDelegationContactDetails {
    delegation {
      id
      email
      address
    }
  }
`);

export const DELEGATION_MEMBERS = gql(/* GraphQL */ `
  query getDelegationMembers {
    delegation {
      studentSet {
        submitted
        code
        givenName
        lastName
        photo
      }
      leaderSet {
        type
        submitted
        code
        givenName
        lastName
        photo
      }
    }
  }
`);

export const DELEGATION_TRANSACTIONS = gql(/* GraphQL */ `
  query getDelegationTransactions {
    delegation {
      transactionSet {
        date
        description
        amount
      }
    }
  }
`);

export const STUDENT = gql(/* GraphQL */ `
  query getStudent($code: String!) {
    student(code: $code) {
      id
      submitted
      code
      givenName
      lastName
      certificateName
      gender
      birthday
      email
      emailAuthorization
      mobileNumber
      schoolName
      schoolAddress
      tshirtSize
      photo
      photographyConsent
      codeOfConduct
      medicalIssue
    }
  }
`);

export const LEADER = gql(/* GraphQL */ `
  query getLeader($code: String!) {
    leader(code: $code) {
      id
      submitted
      code
      givenName
      lastName
      namePrefix
      gender
      birthday
      email
      emailAuthorization
      mobileNumber
      residenceCountry
      residenceAddress
      tshirtSize
      photo
      photographyConsent
      codeOfConduct
      medicalIssue
    }
  }
`);

export const DOCUMENTS = gql(/* GraphQL */ `
  query getDocuments {
    downloads {
      id
      name
      type
      desciption
      icon
      document
    }
  }
`);

export const DELEGATION_DOCUMENTS = gql(/* GraphQL */ `
  query getDocumentTypes {
    documentTypes {
      id
      order
      name
      desciption
      provider
      mandatory
      multiple
      delegationdocumentSet {
        id
        timestamp
        document
      }
    }
  }
`);

// export const DELEGATION_DOCUMENTS = gql(/* GraphQL */ `
//   query getDelegationDocuments {
//     delegation {
//       delegationdocumentSet {
//         timestamp
//         document
//         type {
//           id
//           name
//           provider
//         }
//       }
//     }
//   }
// `);

export const LOGGED_IN_USER = gql(/* GraphQL */ `
  query User {
    me {
      username
      delegationSet {
        name
        country
      }
    }
  }
`);
