// import gql from "graphql-tag";
import { gql } from "@app/gql";

export const LOGIN_USER = gql(/* GraphQL */ `
  mutation login($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`);

export const REFRESH_TOKEN = gql(/* GraphQL */ `
  mutation refreshToken($token: String!) {
    refreshToken(token: $token) {
      token
    }
  }
`);

export const FORGOT_PASSWORD = gql(/* GraphQL */ `
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email) {
      success
    }
  }
`);

export const UPDATE_USER_PASSWORD = gql(/* GraphQL */ `
  mutation updateUserPassword($newPassword1: String!, $newPassword2: String!) {
    updatePassword(
      input: { newPassword1: $newPassword1, newPassword2: $newPassword2 }
    ) {
      errors {
        field
        messages
      }
    }
  }
`);

export const UPDATE_DELEGATION_CONTACT = gql(/* GraphQL */ `
  mutation updateContactDetails($input: UpdateDelegationContactMutationInput!) {
    updateContactDetails(input: $input) {
      delegation {
        id
        email
        address
      }
      errors {
        field
        messages
      }
    }
  }
`);

export const CREATE_STUDENT = gql(/* GraphQL */ `
  mutation createStudent {
    createStudent {
      student {
        code
        givenName
        lastName
        photo
      }
    }
  }
`);

export const REMOVE_STUDENT = gql(/* GraphQL */ `
  mutation removeStudent {
    removeStudent {
      success
    }
  }
`);

export const UPDATE_STUDENT = gql(/* GraphQL */ `
  mutation updateStudent($input: UpdateStudentMutationInput!) {
    updateStudent(input: $input) {
      student {
        code
        givenName
        lastName
        certificateName
      }
      errors {
        field
        messages
      }
    }
  }
`);

export const SUBMIT_STUDENT = gql(/* GraphQL */ `
  mutation submitStudent($code: String!) {
    submitStudent(code: $code) {
      success
    }
  }
`);

export const CREATE_LEADER = gql(/* GraphQL */ `
  mutation createLeader($type: LeaderTypeChoice!) {
    createLeader(type: $type) {
      leader {
        code
        givenName
        lastName
        photo
      }
    }
  }
`);

export const REMOVE_LEADER = gql(/* GraphQL */ `
  mutation removeLeader($type: LeaderTypeChoice!) {
    removeLeader(type: $type) {
      success
    }
  }
`);

export const UPDATE_LEADER = gql(/* GraphQL */ `
  mutation updateLeader($input: UpdateLeaderMutationInput!) {
    updateLeader(input: $input) {
      leader {
        code
        givenName
        lastName
      }
      errors {
        field
        messages
      }
    }
  }
`);

export const SUBMIT_LEADER = gql(/* GraphQL */ `
  mutation submitLeader($code: String!) {
    submitLeader(code: $code) {
      success
    }
  }
`);

export const UPLOAD_DELEGATION_DOCUMENT = gql(/* GraphQL */ `
  mutation uploadDelegationdocument(
    $delegationCode: String!
    $documentType: ID!
    $document: Upload!
  ) {
    uploadDelegationdocument(
      delegationCode: $delegationCode
      documentType: $documentType
      document: $document
    ) {
      success
    }
  }
`);
