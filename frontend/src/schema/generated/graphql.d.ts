import gql from 'graphql-tag';
import { createMutationFunction, createSmartQueryOptionsFunction, createSmartSubscriptionOptionsFunction } from 'vue-apollo-smart-ops';
import { ApolloError } from 'apollo-client';
import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `Date` scalar type represents a Date
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  Date: any;
  /** The `Decimal` scalar type represents a python Decimal. */
  Decimal: any;
  /**
   * The `GenericScalar` scalar type represents a generic
   * GraphQL scalar value that could be:
   * String, Boolean, Int, Float, List or Object.
   */
  GenericScalar: any;
  /**
   * Create scalar that ignores normal serialization/deserialization, since
   * that will be handled by the multipart request spec
   */
  Upload: any;
};

export type CreateLeader = {
  __typename?: 'CreateLeader';
  leader?: Maybe<LeaderType>;
};

export type CreateStudent = {
  __typename?: 'CreateStudent';
  student?: Maybe<StudentType>;
};

export type DelegationDocumentType = {
  __typename?: 'DelegationDocumentType';
  delegation: DelegationType;
  document: Scalars['String'];
  id: Scalars['ID'];
  timestamp: Scalars['Date'];
  type: DelegationDocumentTypeType;
};

export type DelegationDocumentTypeType = {
  __typename?: 'DelegationDocumentTypeType';
  delegationdocumentSet: Array<DelegationDocumentType>;
  desciption: Scalars['String'];
  id: Scalars['ID'];
  mandatory: Scalars['Boolean'];
  multiple: Scalars['Boolean'];
  name: Scalars['String'];
  order: Scalars['Int'];
  provider: OlyRegisterDelegationDocumentTypeProviderChoices;
};

export type DelegationType = {
  __typename?: 'DelegationType';
  address: Scalars['String'];
  country: Scalars['String'];
  delegationUser: UserType;
  delegationdocumentSet: Array<DelegationDocumentType>;
  email: Scalars['String'];
  id: Scalars['ID'];
  leaderSet: Array<LeaderType>;
  name: Scalars['String'];
  studentSet: Array<StudentType>;
  transactionSet: Array<TransactionType>;
};

export type DownloadType = {
  __typename?: 'DownloadType';
  desciption: Scalars['String'];
  document: Scalars['String'];
  icon: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  type: Scalars['String'];
};

export type ErrorType = {
  __typename?: 'ErrorType';
  field: Scalars['String'];
  messages: Array<Scalars['String']>;
};

export type ForgotPassword = {
  __typename?: 'ForgotPassword';
  success?: Maybe<Scalars['String']>;
};

export type LeaderType = {
  __typename?: 'LeaderType';
  birthday?: Maybe<Scalars['Date']>;
  code: Scalars['String'];
  codeOfConduct: Scalars['String'];
  delegation: DelegationType;
  email: Scalars['String'];
  emailAuthorization: Scalars['Boolean'];
  experimentAddress: Scalars['String'];
  gender?: Maybe<OlyRegisterLeaderGenderChoices>;
  givenName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  medicalIssue: Scalars['String'];
  mobileNumber: Scalars['String'];
  namePrefix: Scalars['String'];
  photo: Scalars['String'];
  photographyConsent: Scalars['String'];
  residenceAddress: Scalars['String'];
  residenceCountry: Scalars['String'];
  submitted: Scalars['Boolean'];
  tshirtSize?: Maybe<OlyRegisterLeaderTshirtSizeChoices>;
  type: OlyRegisterLeaderTypeChoices;
};

export enum LeaderTypeChoice {
  /** Invigilator */
  I = 'I',
  /** Team leader */
  L = 'L',
  /** Observer */
  O = 'O'
}

export type Mutation = {
  __typename?: 'Mutation';
  createLeader?: Maybe<CreateLeader>;
  createStudent?: Maybe<CreateStudent>;
  forgotPassword?: Maybe<ForgotPassword>;
  refreshToken?: Maybe<Refresh>;
  removeLeader?: Maybe<RemoveLeader>;
  removeStudent?: Maybe<RemoveStudent>;
  submitLeader?: Maybe<SubmitLeader>;
  submitStudent?: Maybe<SubmitStudent>;
  /** Obtain JSON Web Token mutation */
  tokenAuth?: Maybe<ObtainJsonWebToken>;
  updateContactDetails?: Maybe<UpdateDelegationContactMutationPayload>;
  updateLeader?: Maybe<UpdateLeaderMutationPayload>;
  updatePassword?: Maybe<UpdatePasswordPayload>;
  updateStudent?: Maybe<UpdateStudentMutationPayload>;
  uploadDelegationdocument?: Maybe<UploadDelegationDocument>;
  verifyToken?: Maybe<Verify>;
};


export type MutationCreateLeaderArgs = {
  type: LeaderTypeChoice;
};


export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
};


export type MutationRefreshTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};


export type MutationRemoveLeaderArgs = {
  type: LeaderTypeChoice;
};


export type MutationSubmitLeaderArgs = {
  code: Scalars['String'];
};


export type MutationSubmitStudentArgs = {
  code: Scalars['String'];
};


export type MutationTokenAuthArgs = {
  password: Scalars['String'];
  username: Scalars['String'];
};


export type MutationUpdateContactDetailsArgs = {
  input: UpdateDelegationContactMutationInput;
};


export type MutationUpdateLeaderArgs = {
  input: UpdateLeaderMutationInput;
};


export type MutationUpdatePasswordArgs = {
  input: UpdatePasswordInput;
};


export type MutationUpdateStudentArgs = {
  input: UpdateStudentMutationInput;
};


export type MutationUploadDelegationdocumentArgs = {
  delegationCode: Scalars['String'];
  document: Scalars['Upload'];
  documentType: Scalars['ID'];
};


export type MutationVerifyTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};

/** Obtain JSON Web Token mutation */
export type ObtainJsonWebToken = {
  __typename?: 'ObtainJSONWebToken';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

/** An enumeration. */
export enum OlyRegisterDelegationDocumentTypeProviderChoices {
  /** Delegation */
  D = 'D',
  /** Organizer */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterLeaderGenderChoices {
  /** Female */
  F = 'F',
  /** Male */
  M = 'M',
  /** Other */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterLeaderTshirtSizeChoices {
  /** Large */
  L = 'L',
  /** Medium */
  M = 'M',
  /** Small */
  S = 'S',
  /** X-Large */
  Xl = 'XL'
}

/** An enumeration. */
export enum OlyRegisterLeaderTypeChoices {
  /** Invigilator */
  I = 'I',
  /** Team leader */
  L = 'L',
  /** Observer */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterStudentGenderChoices {
  /** Female */
  F = 'F',
  /** Male */
  M = 'M',
  /** Other */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterStudentTshirtSizeChoices {
  /** Large */
  L = 'L',
  /** Medium */
  M = 'M',
  /** Small */
  S = 'S',
  /** X-Large */
  Xl = 'XL'
}

export type Query = {
  __typename?: 'Query';
  allDelegations?: Maybe<Array<DelegationType>>;
  delegation?: Maybe<DelegationType>;
  delegationAdmin?: Maybe<DelegationType>;
  documentTypes?: Maybe<Array<DelegationDocumentTypeType>>;
  downloads?: Maybe<Array<DownloadType>>;
  leader?: Maybe<LeaderType>;
  me?: Maybe<UserType>;
  student?: Maybe<StudentType>;
};


export type QueryDelegationAdminArgs = {
  name: Scalars['String'];
};


export type QueryLeaderArgs = {
  code: Scalars['String'];
};


export type QueryStudentArgs = {
  code: Scalars['String'];
};

export type Refresh = {
  __typename?: 'Refresh';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

export type RemoveLeader = {
  __typename?: 'RemoveLeader';
  success?: Maybe<Scalars['String']>;
};

export type RemoveStudent = {
  __typename?: 'RemoveStudent';
  success?: Maybe<Scalars['String']>;
};

export type StudentType = {
  __typename?: 'StudentType';
  birthday?: Maybe<Scalars['Date']>;
  certificateName: Scalars['String'];
  code: Scalars['String'];
  codeOfConduct: Scalars['String'];
  delegation: DelegationType;
  email: Scalars['String'];
  emailAuthorization: Scalars['Boolean'];
  gender?: Maybe<OlyRegisterStudentGenderChoices>;
  givenName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  medicalIssue: Scalars['String'];
  mobileNumber: Scalars['String'];
  photo: Scalars['String'];
  photographyConsent: Scalars['String'];
  schoolAddress: Scalars['String'];
  schoolName: Scalars['String'];
  submitted: Scalars['Boolean'];
  tshirtSize?: Maybe<OlyRegisterStudentTshirtSizeChoices>;
};

export type SubmitLeader = {
  __typename?: 'SubmitLeader';
  success?: Maybe<Scalars['String']>;
};

export type SubmitStudent = {
  __typename?: 'SubmitStudent';
  success?: Maybe<Scalars['String']>;
};

export type TransactionType = {
  __typename?: 'TransactionType';
  amount: Scalars['Decimal'];
  date: Scalars['Date'];
  delegation: DelegationType;
  description: Scalars['String'];
  id: Scalars['ID'];
};

export type UpdateDelegationContactMutationInput = {
  address?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type UpdateDelegationContactMutationPayload = {
  __typename?: 'UpdateDelegationContactMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  delegation?: Maybe<DelegationType>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type UpdateLeaderMutationInput = {
  birthday?: InputMaybe<Scalars['Date']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  codeOfConduct?: InputMaybe<Scalars['Upload']>;
  email?: InputMaybe<Scalars['String']>;
  emailAuthorization?: InputMaybe<Scalars['Boolean']>;
  experimentAddress?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Scalars['String']>;
  givenName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lastName?: InputMaybe<Scalars['String']>;
  medicalIssue?: InputMaybe<Scalars['String']>;
  mobileNumber?: InputMaybe<Scalars['String']>;
  namePrefix?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['Upload']>;
  photographyConsent?: InputMaybe<Scalars['Upload']>;
  residenceAddress?: InputMaybe<Scalars['String']>;
  residenceCountry?: InputMaybe<Scalars['String']>;
  tshirtSize?: InputMaybe<Scalars['String']>;
};

export type UpdateLeaderMutationPayload = {
  __typename?: 'UpdateLeaderMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  leader?: Maybe<LeaderType>;
};

export type UpdatePasswordInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** <ul><li>Your password can’t be too similar to your other personal information.</li><li>Your password must contain at least 8 characters.</li><li>Your password can’t be a commonly used password.</li><li>Your password can’t be entirely numeric.</li></ul> */
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
};

export type UpdatePasswordPayload = {
  __typename?: 'UpdatePasswordPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  /** <ul><li>Your password can’t be too similar to your other personal information.</li><li>Your password must contain at least 8 characters.</li><li>Your password can’t be a commonly used password.</li><li>Your password can’t be entirely numeric.</li></ul> */
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
};

export type UpdateStudentMutationInput = {
  birthday?: InputMaybe<Scalars['Date']>;
  certificateName?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  codeOfConduct?: InputMaybe<Scalars['Upload']>;
  email?: InputMaybe<Scalars['String']>;
  emailAuthorization?: InputMaybe<Scalars['Boolean']>;
  gender?: InputMaybe<Scalars['String']>;
  givenName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lastName?: InputMaybe<Scalars['String']>;
  medicalIssue?: InputMaybe<Scalars['String']>;
  mobileNumber?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['Upload']>;
  photographyConsent?: InputMaybe<Scalars['Upload']>;
  schoolAddress?: InputMaybe<Scalars['String']>;
  schoolName?: InputMaybe<Scalars['String']>;
  tshirtSize?: InputMaybe<Scalars['String']>;
};

export type UpdateStudentMutationPayload = {
  __typename?: 'UpdateStudentMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  student?: Maybe<StudentType>;
};

export type UploadDelegationDocument = {
  __typename?: 'UploadDelegationDocument';
  success?: Maybe<Scalars['String']>;
};

export type UserType = {
  __typename?: 'UserType';
  delegationSet: Array<DelegationType>;
  /** Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. */
  username: Scalars['String'];
};

export type Verify = {
  __typename?: 'Verify';
  payload: Scalars['GenericScalar'];
};

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', tokenAuth?: { __typename?: 'ObtainJSONWebToken', token: string } | null };

export type RefreshTokenMutationVariables = Exact<{
  token: Scalars['String'];
}>;


export type RefreshTokenMutation = { __typename?: 'Mutation', refreshToken?: { __typename?: 'Refresh', token: string } | null };

export type ForgotPasswordMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type ForgotPasswordMutation = { __typename?: 'Mutation', forgotPassword?: { __typename?: 'ForgotPassword', success?: string | null } | null };

export type UpdateUserPasswordMutationVariables = Exact<{
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
}>;


export type UpdateUserPasswordMutation = { __typename?: 'Mutation', updatePassword?: { __typename?: 'UpdatePasswordPayload', errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type UpdateContactDetailsMutationVariables = Exact<{
  input: UpdateDelegationContactMutationInput;
}>;


export type UpdateContactDetailsMutation = { __typename?: 'Mutation', updateContactDetails?: { __typename?: 'UpdateDelegationContactMutationPayload', delegation?: { __typename?: 'DelegationType', id: string, email: string, address: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type CreateStudentMutationVariables = Exact<{ [key: string]: never; }>;


export type CreateStudentMutation = { __typename?: 'Mutation', createStudent?: { __typename?: 'CreateStudent', student?: { __typename?: 'StudentType', code: string, givenName: string, lastName: string, photo: string } | null } | null };

export type RemoveStudentMutationVariables = Exact<{ [key: string]: never; }>;


export type RemoveStudentMutation = { __typename?: 'Mutation', removeStudent?: { __typename?: 'RemoveStudent', success?: string | null } | null };

export type UpdateStudentMutationVariables = Exact<{
  input: UpdateStudentMutationInput;
}>;


export type UpdateStudentMutation = { __typename?: 'Mutation', updateStudent?: { __typename?: 'UpdateStudentMutationPayload', student?: { __typename?: 'StudentType', code: string, givenName: string, lastName: string, certificateName: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type SubmitStudentMutationVariables = Exact<{
  code: Scalars['String'];
}>;


export type SubmitStudentMutation = { __typename?: 'Mutation', submitStudent?: { __typename?: 'SubmitStudent', success?: string | null } | null };

export type CreateLeaderMutationVariables = Exact<{
  type: LeaderTypeChoice;
}>;


export type CreateLeaderMutation = { __typename?: 'Mutation', createLeader?: { __typename?: 'CreateLeader', leader?: { __typename?: 'LeaderType', code: string, givenName: string, lastName: string, photo: string } | null } | null };

export type RemoveLeaderMutationVariables = Exact<{
  type: LeaderTypeChoice;
}>;


export type RemoveLeaderMutation = { __typename?: 'Mutation', removeLeader?: { __typename?: 'RemoveLeader', success?: string | null } | null };

export type UpdateLeaderMutationVariables = Exact<{
  input: UpdateLeaderMutationInput;
}>;


export type UpdateLeaderMutation = { __typename?: 'Mutation', updateLeader?: { __typename?: 'UpdateLeaderMutationPayload', leader?: { __typename?: 'LeaderType', code: string, givenName: string, lastName: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type SubmitLeaderMutationVariables = Exact<{
  code: Scalars['String'];
}>;


export type SubmitLeaderMutation = { __typename?: 'Mutation', submitLeader?: { __typename?: 'SubmitLeader', success?: string | null } | null };

export type UploadDelegationdocumentMutationVariables = Exact<{
  delegationCode: Scalars['String'];
  documentType: Scalars['ID'];
  document: Scalars['Upload'];
}>;


export type UploadDelegationdocumentMutation = { __typename?: 'Mutation', uploadDelegationdocument?: { __typename?: 'UploadDelegationDocument', success?: string | null } | null };

export type GetDelegationContactDetailsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationContactDetailsQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', id: string, email: string, address: string } | null };

export type GetDelegationMembersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationMembersQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', studentSet: Array<{ __typename?: 'StudentType', submitted: boolean, code: string, givenName: string, lastName: string, photo: string }>, leaderSet: Array<{ __typename?: 'LeaderType', type: OlyRegisterLeaderTypeChoices, submitted: boolean, code: string, givenName: string, lastName: string, photo: string }> } | null };

export type GetDelegationTransactionsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationTransactionsQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', transactionSet: Array<{ __typename?: 'TransactionType', date: any, description: string, amount: any }> } | null };

export type GetStudentQueryVariables = Exact<{
  code: Scalars['String'];
}>;


export type GetStudentQuery = { __typename?: 'Query', student?: { __typename?: 'StudentType', id: string, submitted: boolean, code: string, givenName: string, lastName: string, certificateName: string, gender?: OlyRegisterStudentGenderChoices | null, birthday?: any | null, email: string, emailAuthorization: boolean, mobileNumber: string, schoolName: string, schoolAddress: string, tshirtSize?: OlyRegisterStudentTshirtSizeChoices | null, photo: string, photographyConsent: string, codeOfConduct: string, medicalIssue: string } | null };

export type GetLeaderQueryVariables = Exact<{
  code: Scalars['String'];
}>;


export type GetLeaderQuery = { __typename?: 'Query', leader?: { __typename?: 'LeaderType', id: string, submitted: boolean, code: string, givenName: string, lastName: string, namePrefix: string, gender?: OlyRegisterLeaderGenderChoices | null, birthday?: any | null, email: string, emailAuthorization: boolean, mobileNumber: string, residenceCountry: string, residenceAddress: string, tshirtSize?: OlyRegisterLeaderTshirtSizeChoices | null, photo: string, photographyConsent: string, codeOfConduct: string, medicalIssue: string } | null };

export type GetDocumentsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDocumentsQuery = { __typename?: 'Query', downloads?: Array<{ __typename?: 'DownloadType', id: string, name: string, type: string, desciption: string, icon: string, document: string }> | null };

export type GetDocumentTypesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDocumentTypesQuery = { __typename?: 'Query', documentTypes?: Array<{ __typename?: 'DelegationDocumentTypeType', id: string, order: number, name: string, desciption: string, provider: OlyRegisterDelegationDocumentTypeProviderChoices, mandatory: boolean, multiple: boolean, delegationdocumentSet: Array<{ __typename?: 'DelegationDocumentType', id: string, timestamp: any, document: string }> }> | null };

export type UserQueryVariables = Exact<{ [key: string]: never; }>;


export type UserQuery = { __typename?: 'Query', me?: { __typename?: 'UserType', username: string, delegationSet: Array<{ __typename?: 'DelegationType', name: string, country: string }> } | null };


export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  tokenAuth(username: $username, password: $password) {
    token
  }
}
    `;
export const RefreshTokenDocument = gql`
    mutation refreshToken($token: String!) {
  refreshToken(token: $token) {
    token
  }
}
    `;
export const ForgotPasswordDocument = gql`
    mutation forgotPassword($email: String!) {
  forgotPassword(email: $email) {
    success
  }
}
    `;
export const UpdateUserPasswordDocument = gql`
    mutation updateUserPassword($newPassword1: String!, $newPassword2: String!) {
  updatePassword(
    input: {newPassword1: $newPassword1, newPassword2: $newPassword2}
  ) {
    errors {
      field
      messages
    }
  }
}
    `;
export const UpdateContactDetailsDocument = gql`
    mutation updateContactDetails($input: UpdateDelegationContactMutationInput!) {
  updateContactDetails(input: $input) {
    delegation {
      id
      email
      address
    }
    errors {
      field
      messages
    }
  }
}
    `;
export const CreateStudentDocument = gql`
    mutation createStudent {
  createStudent {
    student {
      code
      givenName
      lastName
      photo
    }
  }
}
    `;
export const RemoveStudentDocument = gql`
    mutation removeStudent {
  removeStudent {
    success
  }
}
    `;
export const UpdateStudentDocument = gql`
    mutation updateStudent($input: UpdateStudentMutationInput!) {
  updateStudent(input: $input) {
    student {
      code
      givenName
      lastName
      certificateName
    }
    errors {
      field
      messages
    }
  }
}
    `;
export const SubmitStudentDocument = gql`
    mutation submitStudent($code: String!) {
  submitStudent(code: $code) {
    success
  }
}
    `;
export const CreateLeaderDocument = gql`
    mutation createLeader($type: LeaderTypeChoice!) {
  createLeader(type: $type) {
    leader {
      code
      givenName
      lastName
      photo
    }
  }
}
    `;
export const RemoveLeaderDocument = gql`
    mutation removeLeader($type: LeaderTypeChoice!) {
  removeLeader(type: $type) {
    success
  }
}
    `;
export const UpdateLeaderDocument = gql`
    mutation updateLeader($input: UpdateLeaderMutationInput!) {
  updateLeader(input: $input) {
    leader {
      code
      givenName
      lastName
    }
    errors {
      field
      messages
    }
  }
}
    `;
export const SubmitLeaderDocument = gql`
    mutation submitLeader($code: String!) {
  submitLeader(code: $code) {
    success
  }
}
    `;
export const UploadDelegationdocumentDocument = gql`
    mutation uploadDelegationdocument($delegationCode: String!, $documentType: ID!, $document: Upload!) {
  uploadDelegationdocument(
    delegationCode: $delegationCode
    documentType: $documentType
    document: $document
  ) {
    success
  }
}
    `;
export const GetDelegationContactDetailsDocument = gql`
    query getDelegationContactDetails {
  delegation {
    id
    email
    address
  }
}
    `;
export const GetDelegationMembersDocument = gql`
    query getDelegationMembers {
  delegation {
    studentSet {
      submitted
      code
      givenName
      lastName
      photo
    }
    leaderSet {
      type
      submitted
      code
      givenName
      lastName
      photo
    }
  }
}
    `;
export const GetDelegationTransactionsDocument = gql`
    query getDelegationTransactions {
  delegation {
    transactionSet {
      date
      description
      amount
    }
  }
}
    `;
export const GetStudentDocument = gql`
    query getStudent($code: String!) {
  student(code: $code) {
    id
    submitted
    code
    givenName
    lastName
    certificateName
    gender
    birthday
    email
    emailAuthorization
    mobileNumber
    schoolName
    schoolAddress
    tshirtSize
    photo
    photographyConsent
    codeOfConduct
    medicalIssue
  }
}
    `;
export const GetLeaderDocument = gql`
    query getLeader($code: String!) {
  leader(code: $code) {
    id
    submitted
    code
    givenName
    lastName
    namePrefix
    gender
    birthday
    email
    emailAuthorization
    mobileNumber
    residenceCountry
    residenceAddress
    tshirtSize
    photo
    photographyConsent
    codeOfConduct
    medicalIssue
  }
}
    `;
export const GetDocumentsDocument = gql`
    query getDocuments {
  downloads {
    id
    name
    type
    desciption
    icon
    document
  }
}
    `;
export const GetDocumentTypesDocument = gql`
    query getDocumentTypes {
  documentTypes {
    id
    order
    name
    desciption
    provider
    mandatory
    multiple
    delegationdocumentSet {
      id
      timestamp
      document
    }
  }
}
    `;
export const UserDocument = gql`
    query User {
  me {
    username
    delegationSet {
      name
      country
    }
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string, operationType?: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName, _operationType) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    login(variables: LoginMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoginMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoginMutation>(LoginDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'login', 'mutation');
    },
    refreshToken(variables: RefreshTokenMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RefreshTokenMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<RefreshTokenMutation>(RefreshTokenDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'refreshToken', 'mutation');
    },
    forgotPassword(variables: ForgotPasswordMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ForgotPasswordMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<ForgotPasswordMutation>(ForgotPasswordDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'forgotPassword', 'mutation');
    },
    updateUserPassword(variables: UpdateUserPasswordMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateUserPasswordMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UpdateUserPasswordMutation>(UpdateUserPasswordDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'updateUserPassword', 'mutation');
    },
    updateContactDetails(variables: UpdateContactDetailsMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateContactDetailsMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UpdateContactDetailsMutation>(UpdateContactDetailsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'updateContactDetails', 'mutation');
    },
    createStudent(variables?: CreateStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateStudentMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<CreateStudentMutation>(CreateStudentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'createStudent', 'mutation');
    },
    removeStudent(variables?: RemoveStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RemoveStudentMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<RemoveStudentMutation>(RemoveStudentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'removeStudent', 'mutation');
    },
    updateStudent(variables: UpdateStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateStudentMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UpdateStudentMutation>(UpdateStudentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'updateStudent', 'mutation');
    },
    submitStudent(variables: SubmitStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<SubmitStudentMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<SubmitStudentMutation>(SubmitStudentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'submitStudent', 'mutation');
    },
    createLeader(variables: CreateLeaderMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateLeaderMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<CreateLeaderMutation>(CreateLeaderDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'createLeader', 'mutation');
    },
    removeLeader(variables: RemoveLeaderMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RemoveLeaderMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<RemoveLeaderMutation>(RemoveLeaderDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'removeLeader', 'mutation');
    },
    updateLeader(variables: UpdateLeaderMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateLeaderMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UpdateLeaderMutation>(UpdateLeaderDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'updateLeader', 'mutation');
    },
    submitLeader(variables: SubmitLeaderMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<SubmitLeaderMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<SubmitLeaderMutation>(SubmitLeaderDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'submitLeader', 'mutation');
    },
    uploadDelegationdocument(variables: UploadDelegationdocumentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UploadDelegationdocumentMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UploadDelegationdocumentMutation>(UploadDelegationdocumentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'uploadDelegationdocument', 'mutation');
    },
    getDelegationContactDetails(variables?: GetDelegationContactDetailsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetDelegationContactDetailsQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetDelegationContactDetailsQuery>(GetDelegationContactDetailsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getDelegationContactDetails', 'query');
    },
    getDelegationMembers(variables?: GetDelegationMembersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetDelegationMembersQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetDelegationMembersQuery>(GetDelegationMembersDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getDelegationMembers', 'query');
    },
    getDelegationTransactions(variables?: GetDelegationTransactionsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetDelegationTransactionsQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetDelegationTransactionsQuery>(GetDelegationTransactionsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getDelegationTransactions', 'query');
    },
    getStudent(variables: GetStudentQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetStudentQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetStudentQuery>(GetStudentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getStudent', 'query');
    },
    getLeader(variables: GetLeaderQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetLeaderQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetLeaderQuery>(GetLeaderDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getLeader', 'query');
    },
    getDocuments(variables?: GetDocumentsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetDocumentsQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetDocumentsQuery>(GetDocumentsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getDocuments', 'query');
    },
    getDocumentTypes(variables?: GetDocumentTypesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetDocumentTypesQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetDocumentTypesQuery>(GetDocumentTypesDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getDocumentTypes', 'query');
    },
    User(variables?: UserQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserQuery>(UserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'User', 'query');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;

export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  tokenAuth(username: $username, password: $password) {
    token
  }
}
    `;

/**
 * __loginMutation__
 *
 * To run a mutation, you call `loginMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = loginMutation(this, {
 *   variables: {
 *     username: // value for 'username'
 *     password: // value for 'password'
 *   },
 * });
 */
export const loginMutation = createMutationFunction<
  LoginMutation,
  LoginMutationVariables,
  ApolloError
>(LoginDocument);

export const RefreshTokenDocument = gql`
    mutation refreshToken($token: String!) {
  refreshToken(token: $token) {
    token
  }
}
    `;

/**
 * __refreshTokenMutation__
 *
 * To run a mutation, you call `refreshTokenMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = refreshTokenMutation(this, {
 *   variables: {
 *     token: // value for 'token'
 *   },
 * });
 */
export const refreshTokenMutation = createMutationFunction<
  RefreshTokenMutation,
  RefreshTokenMutationVariables,
  ApolloError
>(RefreshTokenDocument);

export const ForgotPasswordDocument = gql`
    mutation forgotPassword($email: String!) {
  forgotPassword(email: $email) {
    success
  }
}
    `;

/**
 * __forgotPasswordMutation__
 *
 * To run a mutation, you call `forgotPasswordMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = forgotPasswordMutation(this, {
 *   variables: {
 *     email: // value for 'email'
 *   },
 * });
 */
export const forgotPasswordMutation = createMutationFunction<
  ForgotPasswordMutation,
  ForgotPasswordMutationVariables,
  ApolloError
>(ForgotPasswordDocument);

export const UpdateUserPasswordDocument = gql`
    mutation updateUserPassword($newPassword1: String!, $newPassword2: String!) {
  updatePassword(
    input: {newPassword1: $newPassword1, newPassword2: $newPassword2}
  ) {
    errors {
      field
      messages
    }
  }
}
    `;

/**
 * __updateUserPasswordMutation__
 *
 * To run a mutation, you call `updateUserPasswordMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = updateUserPasswordMutation(this, {
 *   variables: {
 *     newPassword1: // value for 'newPassword1'
 *     newPassword2: // value for 'newPassword2'
 *   },
 * });
 */
export const updateUserPasswordMutation = createMutationFunction<
  UpdateUserPasswordMutation,
  UpdateUserPasswordMutationVariables,
  ApolloError
>(UpdateUserPasswordDocument);

export const UpdateContactDetailsDocument = gql`
    mutation updateContactDetails($input: UpdateDelegationContactMutationInput!) {
  updateContactDetails(input: $input) {
    delegation {
      id
      email
      address
    }
    errors {
      field
      messages
    }
  }
}
    `;

/**
 * __updateContactDetailsMutation__
 *
 * To run a mutation, you call `updateContactDetailsMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = updateContactDetailsMutation(this, {
 *   variables: {
 *     input: // value for 'input'
 *   },
 * });
 */
export const updateContactDetailsMutation = createMutationFunction<
  UpdateContactDetailsMutation,
  UpdateContactDetailsMutationVariables,
  ApolloError
>(UpdateContactDetailsDocument);

export const CreateStudentDocument = gql`
    mutation createStudent {
  createStudent {
    student {
      code
      givenName
      lastName
      photo
    }
  }
}
    `;

/**
 * __createStudentMutation__
 *
 * To run a mutation, you call `createStudentMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = createStudentMutation(this, {
 *   variables: {},
 * });
 */
export const createStudentMutation = createMutationFunction<
  CreateStudentMutation,
  CreateStudentMutationVariables,
  ApolloError
>(CreateStudentDocument);

export const RemoveStudentDocument = gql`
    mutation removeStudent {
  removeStudent {
    success
  }
}
    `;

/**
 * __removeStudentMutation__
 *
 * To run a mutation, you call `removeStudentMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = removeStudentMutation(this, {
 *   variables: {},
 * });
 */
export const removeStudentMutation = createMutationFunction<
  RemoveStudentMutation,
  RemoveStudentMutationVariables,
  ApolloError
>(RemoveStudentDocument);

export const UpdateStudentDocument = gql`
    mutation updateStudent($input: UpdateStudentMutationInput!) {
  updateStudent(input: $input) {
    student {
      code
      givenName
      lastName
      certificateName
    }
    errors {
      field
      messages
    }
  }
}
    `;

/**
 * __updateStudentMutation__
 *
 * To run a mutation, you call `updateStudentMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = updateStudentMutation(this, {
 *   variables: {
 *     input: // value for 'input'
 *   },
 * });
 */
export const updateStudentMutation = createMutationFunction<
  UpdateStudentMutation,
  UpdateStudentMutationVariables,
  ApolloError
>(UpdateStudentDocument);

export const SubmitStudentDocument = gql`
    mutation submitStudent($code: String!) {
  submitStudent(code: $code) {
    success
  }
}
    `;

/**
 * __submitStudentMutation__
 *
 * To run a mutation, you call `submitStudentMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = submitStudentMutation(this, {
 *   variables: {
 *     code: // value for 'code'
 *   },
 * });
 */
export const submitStudentMutation = createMutationFunction<
  SubmitStudentMutation,
  SubmitStudentMutationVariables,
  ApolloError
>(SubmitStudentDocument);

export const CreateLeaderDocument = gql`
    mutation createLeader($type: LeaderTypeChoice!) {
  createLeader(type: $type) {
    leader {
      code
      givenName
      lastName
      photo
    }
  }
}
    `;

/**
 * __createLeaderMutation__
 *
 * To run a mutation, you call `createLeaderMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = createLeaderMutation(this, {
 *   variables: {
 *     type: // value for 'type'
 *   },
 * });
 */
export const createLeaderMutation = createMutationFunction<
  CreateLeaderMutation,
  CreateLeaderMutationVariables,
  ApolloError
>(CreateLeaderDocument);

export const RemoveLeaderDocument = gql`
    mutation removeLeader($type: LeaderTypeChoice!) {
  removeLeader(type: $type) {
    success
  }
}
    `;

/**
 * __removeLeaderMutation__
 *
 * To run a mutation, you call `removeLeaderMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = removeLeaderMutation(this, {
 *   variables: {
 *     type: // value for 'type'
 *   },
 * });
 */
export const removeLeaderMutation = createMutationFunction<
  RemoveLeaderMutation,
  RemoveLeaderMutationVariables,
  ApolloError
>(RemoveLeaderDocument);

export const UpdateLeaderDocument = gql`
    mutation updateLeader($input: UpdateLeaderMutationInput!) {
  updateLeader(input: $input) {
    leader {
      code
      givenName
      lastName
    }
    errors {
      field
      messages
    }
  }
}
    `;

/**
 * __updateLeaderMutation__
 *
 * To run a mutation, you call `updateLeaderMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = updateLeaderMutation(this, {
 *   variables: {
 *     input: // value for 'input'
 *   },
 * });
 */
export const updateLeaderMutation = createMutationFunction<
  UpdateLeaderMutation,
  UpdateLeaderMutationVariables,
  ApolloError
>(UpdateLeaderDocument);

export const SubmitLeaderDocument = gql`
    mutation submitLeader($code: String!) {
  submitLeader(code: $code) {
    success
  }
}
    `;

/**
 * __submitLeaderMutation__
 *
 * To run a mutation, you call `submitLeaderMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = submitLeaderMutation(this, {
 *   variables: {
 *     code: // value for 'code'
 *   },
 * });
 */
export const submitLeaderMutation = createMutationFunction<
  SubmitLeaderMutation,
  SubmitLeaderMutationVariables,
  ApolloError
>(SubmitLeaderDocument);

export const UploadDelegationdocumentDocument = gql`
    mutation uploadDelegationdocument($delegationCode: String!, $documentType: ID!, $document: Upload!) {
  uploadDelegationdocument(
    delegationCode: $delegationCode
    documentType: $documentType
    document: $document
  ) {
    success
  }
}
    `;

/**
 * __uploadDelegationdocumentMutation__
 *
 * To run a mutation, you call `uploadDelegationdocumentMutation` within a Vue component and pass it
 * your Vue app instance along with any options that fit your needs.
 *
 * @param app, a reference to your Vue app instance (which must have a `$apollo` property)
 * @param options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.mutate
 * @param client (optional), which can be an instance of `DollarApollo` or the `mutate()` function provided by an `<ApolloMutation>` component
 *
 * @example
 * const { success, data, errors } = uploadDelegationdocumentMutation(this, {
 *   variables: {
 *     delegationCode: // value for 'delegationCode'
 *     documentType: // value for 'documentType'
 *     document: // value for 'document'
 *   },
 * });
 */
export const uploadDelegationdocumentMutation = createMutationFunction<
  UploadDelegationdocumentMutation,
  UploadDelegationdocumentMutationVariables,
  ApolloError
>(UploadDelegationdocumentDocument);

export const GetDelegationContactDetailsDocument = gql`
    query getDelegationContactDetails {
  delegation {
    id
    email
    address
  }
}
    `;

/**
 * __useGetDelegationContactDetailsQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetDelegationContactDetailsQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getDelegationContactDetails: useGetDelegationContactDetailsQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetDelegationContactDetailsQuery = createSmartQueryOptionsFunction<
  GetDelegationContactDetailsQuery,
  GetDelegationContactDetailsQueryVariables,
  ApolloError
>(GetDelegationContactDetailsDocument);

export const GetDelegationMembersDocument = gql`
    query getDelegationMembers {
  delegation {
    studentSet {
      submitted
      code
      givenName
      lastName
      photo
    }
    leaderSet {
      type
      submitted
      code
      givenName
      lastName
      photo
    }
  }
}
    `;

/**
 * __useGetDelegationMembersQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetDelegationMembersQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getDelegationMembers: useGetDelegationMembersQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetDelegationMembersQuery = createSmartQueryOptionsFunction<
  GetDelegationMembersQuery,
  GetDelegationMembersQueryVariables,
  ApolloError
>(GetDelegationMembersDocument);

export const GetDelegationTransactionsDocument = gql`
    query getDelegationTransactions {
  delegation {
    transactionSet {
      date
      description
      amount
    }
  }
}
    `;

/**
 * __useGetDelegationTransactionsQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetDelegationTransactionsQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getDelegationTransactions: useGetDelegationTransactionsQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetDelegationTransactionsQuery = createSmartQueryOptionsFunction<
  GetDelegationTransactionsQuery,
  GetDelegationTransactionsQueryVariables,
  ApolloError
>(GetDelegationTransactionsDocument);

export const GetStudentDocument = gql`
    query getStudent($code: String!) {
  student(code: $code) {
    id
    submitted
    code
    givenName
    lastName
    certificateName
    gender
    birthday
    email
    emailAuthorization
    mobileNumber
    schoolName
    schoolAddress
    tshirtSize
    photo
    photographyConsent
    codeOfConduct
    medicalIssue
  }
}
    `;

/**
 * __useGetStudentQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetStudentQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getStudent: useGetStudentQuery({
 *       variables: {
 *         code: // value for 'code'
 *       },
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetStudentQuery = createSmartQueryOptionsFunction<
  GetStudentQuery,
  GetStudentQueryVariables,
  ApolloError
>(GetStudentDocument);

export const GetLeaderDocument = gql`
    query getLeader($code: String!) {
  leader(code: $code) {
    id
    submitted
    code
    givenName
    lastName
    namePrefix
    gender
    birthday
    email
    emailAuthorization
    mobileNumber
    residenceCountry
    residenceAddress
    tshirtSize
    photo
    photographyConsent
    codeOfConduct
    medicalIssue
  }
}
    `;

/**
 * __useGetLeaderQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetLeaderQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getLeader: useGetLeaderQuery({
 *       variables: {
 *         code: // value for 'code'
 *       },
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetLeaderQuery = createSmartQueryOptionsFunction<
  GetLeaderQuery,
  GetLeaderQueryVariables,
  ApolloError
>(GetLeaderDocument);

export const GetDocumentsDocument = gql`
    query getDocuments {
  downloads {
    id
    name
    type
    desciption
    icon
    document
  }
}
    `;

/**
 * __useGetDocumentsQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetDocumentsQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getDocuments: useGetDocumentsQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetDocumentsQuery = createSmartQueryOptionsFunction<
  GetDocumentsQuery,
  GetDocumentsQueryVariables,
  ApolloError
>(GetDocumentsDocument);

export const GetDocumentTypesDocument = gql`
    query getDocumentTypes {
  documentTypes {
    id
    order
    name
    desciption
    provider
    mandatory
    multiple
    delegationdocumentSet {
      id
      timestamp
      document
    }
  }
}
    `;

/**
 * __useGetDocumentTypesQuery__
 *
 * To use a Smart Query within a Vue component, call `useGetDocumentTypesQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     getDocumentTypes: useGetDocumentTypesQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useGetDocumentTypesQuery = createSmartQueryOptionsFunction<
  GetDocumentTypesQuery,
  GetDocumentTypesQueryVariables,
  ApolloError
>(GetDocumentTypesDocument);

export const UserDocument = gql`
    query User {
  me {
    username
    delegationSet {
      name
      country
    }
  }
}
    `;

/**
 * __useUserQuery__
 *
 * To use a Smart Query within a Vue component, call `useUserQuery` as the value for a query key
 * in the component's `apollo` config, passing any options required for the query.
 *
 * @param options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.query
 *
 * @example
 * {
 *   apollo: {
 *     user: useUserQuery({
 *       variables: {},
 *       loadingKey: 'loading',
 *       fetchPolicy: 'no-cache',
 *     }),
 *   }
 * }
 */
export const useUserQuery = createSmartQueryOptionsFunction<
  UserQuery,
  UserQueryVariables,
  ApolloError
>(UserDocument);
