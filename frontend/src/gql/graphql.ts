/* eslint-disable */
import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `Date` scalar type represents a Date
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  Date: any;
  /** The `Decimal` scalar type represents a python Decimal. */
  Decimal: any;
  /**
   * The `GenericScalar` scalar type represents a generic
   * GraphQL scalar value that could be:
   * String, Boolean, Int, Float, List or Object.
   */
  GenericScalar: any;
  /**
   * Create scalar that ignores normal serialization/deserialization, since
   * that will be handled by the multipart request spec
   */
  Upload: any;
};

export type CreateLeader = {
  __typename?: 'CreateLeader';
  leader?: Maybe<LeaderType>;
};

export type CreateStudent = {
  __typename?: 'CreateStudent';
  student?: Maybe<StudentType>;
};

export type DelegationDocumentType = {
  __typename?: 'DelegationDocumentType';
  delegation: DelegationType;
  document: Scalars['String'];
  id: Scalars['ID'];
  timestamp: Scalars['Date'];
  type: DelegationDocumentTypeType;
};

export type DelegationDocumentTypeType = {
  __typename?: 'DelegationDocumentTypeType';
  delegationdocumentSet: Array<DelegationDocumentType>;
  desciption: Scalars['String'];
  id: Scalars['ID'];
  mandatory: Scalars['Boolean'];
  multiple: Scalars['Boolean'];
  name: Scalars['String'];
  order: Scalars['Int'];
  provider: OlyRegisterDelegationDocumentTypeProviderChoices;
};

export type DelegationType = {
  __typename?: 'DelegationType';
  address: Scalars['String'];
  country: Scalars['String'];
  delegationUser: UserType;
  delegationdocumentSet: Array<DelegationDocumentType>;
  email: Scalars['String'];
  id: Scalars['ID'];
  leaderSet: Array<LeaderType>;
  name: Scalars['String'];
  studentSet: Array<StudentType>;
  transactionSet: Array<TransactionType>;
};

export type DownloadType = {
  __typename?: 'DownloadType';
  desciption: Scalars['String'];
  document: Scalars['String'];
  icon: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  type: Scalars['String'];
};

export type ErrorType = {
  __typename?: 'ErrorType';
  field: Scalars['String'];
  messages: Array<Scalars['String']>;
};

export type ForgotPassword = {
  __typename?: 'ForgotPassword';
  success?: Maybe<Scalars['String']>;
};

export type LeaderType = {
  __typename?: 'LeaderType';
  birthday?: Maybe<Scalars['Date']>;
  code: Scalars['String'];
  codeOfConduct: Scalars['String'];
  delegation: DelegationType;
  email: Scalars['String'];
  emailAuthorization: Scalars['Boolean'];
  experimentAddress: Scalars['String'];
  gender?: Maybe<OlyRegisterLeaderGenderChoices>;
  givenName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  medicalIssue: Scalars['String'];
  mobileNumber: Scalars['String'];
  namePrefix: Scalars['String'];
  photo: Scalars['String'];
  photographyConsent: Scalars['String'];
  residenceAddress: Scalars['String'];
  residenceCountry: Scalars['String'];
  submitted: Scalars['Boolean'];
  tshirtSize?: Maybe<OlyRegisterLeaderTshirtSizeChoices>;
  type: OlyRegisterLeaderTypeChoices;
};

export enum LeaderTypeChoice {
  /** Invigilator */
  I = 'I',
  /** Team leader */
  L = 'L',
  /** Observer */
  O = 'O'
}

export type Mutation = {
  __typename?: 'Mutation';
  createLeader?: Maybe<CreateLeader>;
  createStudent?: Maybe<CreateStudent>;
  forgotPassword?: Maybe<ForgotPassword>;
  refreshToken?: Maybe<Refresh>;
  removeLeader?: Maybe<RemoveLeader>;
  removeStudent?: Maybe<RemoveStudent>;
  submitLeader?: Maybe<SubmitLeader>;
  submitStudent?: Maybe<SubmitStudent>;
  /** Obtain JSON Web Token mutation */
  tokenAuth?: Maybe<ObtainJsonWebToken>;
  updateContactDetails?: Maybe<UpdateDelegationContactMutationPayload>;
  updateLeader?: Maybe<UpdateLeaderMutationPayload>;
  updatePassword?: Maybe<UpdatePasswordPayload>;
  updateStudent?: Maybe<UpdateStudentMutationPayload>;
  uploadDelegationdocument?: Maybe<UploadDelegationDocument>;
  verifyToken?: Maybe<Verify>;
};


export type MutationCreateLeaderArgs = {
  type: LeaderTypeChoice;
};


export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
};


export type MutationRefreshTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};


export type MutationRemoveLeaderArgs = {
  type: LeaderTypeChoice;
};


export type MutationSubmitLeaderArgs = {
  code: Scalars['String'];
};


export type MutationSubmitStudentArgs = {
  code: Scalars['String'];
};


export type MutationTokenAuthArgs = {
  password: Scalars['String'];
  username: Scalars['String'];
};


export type MutationUpdateContactDetailsArgs = {
  input: UpdateDelegationContactMutationInput;
};


export type MutationUpdateLeaderArgs = {
  input: UpdateLeaderMutationInput;
};


export type MutationUpdatePasswordArgs = {
  input: UpdatePasswordInput;
};


export type MutationUpdateStudentArgs = {
  input: UpdateStudentMutationInput;
};


export type MutationUploadDelegationdocumentArgs = {
  delegationCode: Scalars['String'];
  document: Scalars['Upload'];
  documentType: Scalars['ID'];
};


export type MutationVerifyTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};

/** Obtain JSON Web Token mutation */
export type ObtainJsonWebToken = {
  __typename?: 'ObtainJSONWebToken';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

/** An enumeration. */
export enum OlyRegisterDelegationDocumentTypeProviderChoices {
  /** Delegation */
  D = 'D',
  /** Organizer */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterLeaderGenderChoices {
  /** Female */
  F = 'F',
  /** Male */
  M = 'M',
  /** Other */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterLeaderTshirtSizeChoices {
  /** Large */
  L = 'L',
  /** Medium */
  M = 'M',
  /** Small */
  S = 'S',
  /** X-Large */
  Xl = 'XL'
}

/** An enumeration. */
export enum OlyRegisterLeaderTypeChoices {
  /** Invigilator */
  I = 'I',
  /** Team leader */
  L = 'L',
  /** Observer */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterStudentGenderChoices {
  /** Female */
  F = 'F',
  /** Male */
  M = 'M',
  /** Other */
  O = 'O'
}

/** An enumeration. */
export enum OlyRegisterStudentTshirtSizeChoices {
  /** Large */
  L = 'L',
  /** Medium */
  M = 'M',
  /** Small */
  S = 'S',
  /** X-Large */
  Xl = 'XL'
}

export type Query = {
  __typename?: 'Query';
  allDelegations?: Maybe<Array<DelegationType>>;
  delegation?: Maybe<DelegationType>;
  delegationAdmin?: Maybe<DelegationType>;
  documentTypes?: Maybe<Array<DelegationDocumentTypeType>>;
  downloads?: Maybe<Array<DownloadType>>;
  leader?: Maybe<LeaderType>;
  me?: Maybe<UserType>;
  student?: Maybe<StudentType>;
};


export type QueryDelegationAdminArgs = {
  name: Scalars['String'];
};


export type QueryLeaderArgs = {
  code: Scalars['String'];
};


export type QueryStudentArgs = {
  code: Scalars['String'];
};

export type Refresh = {
  __typename?: 'Refresh';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

export type RemoveLeader = {
  __typename?: 'RemoveLeader';
  success?: Maybe<Scalars['String']>;
};

export type RemoveStudent = {
  __typename?: 'RemoveStudent';
  success?: Maybe<Scalars['String']>;
};

export type StudentType = {
  __typename?: 'StudentType';
  birthday?: Maybe<Scalars['Date']>;
  certificateName: Scalars['String'];
  code: Scalars['String'];
  codeOfConduct: Scalars['String'];
  delegation: DelegationType;
  email: Scalars['String'];
  emailAuthorization: Scalars['Boolean'];
  gender?: Maybe<OlyRegisterStudentGenderChoices>;
  givenName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  medicalIssue: Scalars['String'];
  mobileNumber: Scalars['String'];
  photo: Scalars['String'];
  photographyConsent: Scalars['String'];
  schoolAddress: Scalars['String'];
  schoolName: Scalars['String'];
  submitted: Scalars['Boolean'];
  tshirtSize?: Maybe<OlyRegisterStudentTshirtSizeChoices>;
};

export type SubmitLeader = {
  __typename?: 'SubmitLeader';
  success?: Maybe<Scalars['String']>;
};

export type SubmitStudent = {
  __typename?: 'SubmitStudent';
  success?: Maybe<Scalars['String']>;
};

export type TransactionType = {
  __typename?: 'TransactionType';
  amount: Scalars['Decimal'];
  date: Scalars['Date'];
  delegation: DelegationType;
  description: Scalars['String'];
  id: Scalars['ID'];
};

export type UpdateDelegationContactMutationInput = {
  address?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type UpdateDelegationContactMutationPayload = {
  __typename?: 'UpdateDelegationContactMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  delegation?: Maybe<DelegationType>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type UpdateLeaderMutationInput = {
  birthday?: InputMaybe<Scalars['Date']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  codeOfConduct?: InputMaybe<Scalars['Upload']>;
  email?: InputMaybe<Scalars['String']>;
  emailAuthorization?: InputMaybe<Scalars['Boolean']>;
  experimentAddress?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Scalars['String']>;
  givenName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lastName?: InputMaybe<Scalars['String']>;
  medicalIssue?: InputMaybe<Scalars['String']>;
  mobileNumber?: InputMaybe<Scalars['String']>;
  namePrefix?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['Upload']>;
  photographyConsent?: InputMaybe<Scalars['Upload']>;
  residenceAddress?: InputMaybe<Scalars['String']>;
  residenceCountry?: InputMaybe<Scalars['String']>;
  tshirtSize?: InputMaybe<Scalars['String']>;
};

export type UpdateLeaderMutationPayload = {
  __typename?: 'UpdateLeaderMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  leader?: Maybe<LeaderType>;
};

export type UpdatePasswordInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** <ul><li>Your password can’t be too similar to your other personal information.</li><li>Your password must contain at least 8 characters.</li><li>Your password can’t be a commonly used password.</li><li>Your password can’t be entirely numeric.</li></ul> */
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
};

export type UpdatePasswordPayload = {
  __typename?: 'UpdatePasswordPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  /** <ul><li>Your password can’t be too similar to your other personal information.</li><li>Your password must contain at least 8 characters.</li><li>Your password can’t be a commonly used password.</li><li>Your password can’t be entirely numeric.</li></ul> */
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
};

export type UpdateStudentMutationInput = {
  birthday?: InputMaybe<Scalars['Date']>;
  certificateName?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  codeOfConduct?: InputMaybe<Scalars['Upload']>;
  email?: InputMaybe<Scalars['String']>;
  emailAuthorization?: InputMaybe<Scalars['Boolean']>;
  gender?: InputMaybe<Scalars['String']>;
  givenName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lastName?: InputMaybe<Scalars['String']>;
  medicalIssue?: InputMaybe<Scalars['String']>;
  mobileNumber?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['Upload']>;
  photographyConsent?: InputMaybe<Scalars['Upload']>;
  schoolAddress?: InputMaybe<Scalars['String']>;
  schoolName?: InputMaybe<Scalars['String']>;
  tshirtSize?: InputMaybe<Scalars['String']>;
};

export type UpdateStudentMutationPayload = {
  __typename?: 'UpdateStudentMutationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  student?: Maybe<StudentType>;
};

export type UploadDelegationDocument = {
  __typename?: 'UploadDelegationDocument';
  success?: Maybe<Scalars['String']>;
};

export type UserType = {
  __typename?: 'UserType';
  delegationSet: Array<DelegationType>;
  /** Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. */
  username: Scalars['String'];
};

export type Verify = {
  __typename?: 'Verify';
  payload: Scalars['GenericScalar'];
};

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', tokenAuth?: { __typename?: 'ObtainJSONWebToken', token: string } | null };

export type RefreshTokenMutationVariables = Exact<{
  token: Scalars['String'];
}>;


export type RefreshTokenMutation = { __typename?: 'Mutation', refreshToken?: { __typename?: 'Refresh', token: string } | null };

export type ForgotPasswordMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type ForgotPasswordMutation = { __typename?: 'Mutation', forgotPassword?: { __typename?: 'ForgotPassword', success?: string | null } | null };

export type UpdateUserPasswordMutationVariables = Exact<{
  newPassword1: Scalars['String'];
  newPassword2: Scalars['String'];
}>;


export type UpdateUserPasswordMutation = { __typename?: 'Mutation', updatePassword?: { __typename?: 'UpdatePasswordPayload', errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type UpdateContactDetailsMutationVariables = Exact<{
  input: UpdateDelegationContactMutationInput;
}>;


export type UpdateContactDetailsMutation = { __typename?: 'Mutation', updateContactDetails?: { __typename?: 'UpdateDelegationContactMutationPayload', delegation?: { __typename?: 'DelegationType', id: string, email: string, address: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type CreateStudentMutationVariables = Exact<{ [key: string]: never; }>;


export type CreateStudentMutation = { __typename?: 'Mutation', createStudent?: { __typename?: 'CreateStudent', student?: { __typename?: 'StudentType', code: string, givenName: string, lastName: string, photo: string } | null } | null };

export type RemoveStudentMutationVariables = Exact<{ [key: string]: never; }>;


export type RemoveStudentMutation = { __typename?: 'Mutation', removeStudent?: { __typename?: 'RemoveStudent', success?: string | null } | null };

export type UpdateStudentMutationVariables = Exact<{
  input: UpdateStudentMutationInput;
}>;


export type UpdateStudentMutation = { __typename?: 'Mutation', updateStudent?: { __typename?: 'UpdateStudentMutationPayload', student?: { __typename?: 'StudentType', code: string, givenName: string, lastName: string, certificateName: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type SubmitStudentMutationVariables = Exact<{
  code: Scalars['String'];
}>;


export type SubmitStudentMutation = { __typename?: 'Mutation', submitStudent?: { __typename?: 'SubmitStudent', success?: string | null } | null };

export type CreateLeaderMutationVariables = Exact<{
  type: LeaderTypeChoice;
}>;


export type CreateLeaderMutation = { __typename?: 'Mutation', createLeader?: { __typename?: 'CreateLeader', leader?: { __typename?: 'LeaderType', code: string, givenName: string, lastName: string, photo: string } | null } | null };

export type RemoveLeaderMutationVariables = Exact<{
  type: LeaderTypeChoice;
}>;


export type RemoveLeaderMutation = { __typename?: 'Mutation', removeLeader?: { __typename?: 'RemoveLeader', success?: string | null } | null };

export type UpdateLeaderMutationVariables = Exact<{
  input: UpdateLeaderMutationInput;
}>;


export type UpdateLeaderMutation = { __typename?: 'Mutation', updateLeader?: { __typename?: 'UpdateLeaderMutationPayload', leader?: { __typename?: 'LeaderType', code: string, givenName: string, lastName: string } | null, errors?: Array<{ __typename?: 'ErrorType', field: string, messages: Array<string> } | null> | null } | null };

export type SubmitLeaderMutationVariables = Exact<{
  code: Scalars['String'];
}>;


export type SubmitLeaderMutation = { __typename?: 'Mutation', submitLeader?: { __typename?: 'SubmitLeader', success?: string | null } | null };

export type UploadDelegationdocumentMutationVariables = Exact<{
  delegationCode: Scalars['String'];
  documentType: Scalars['ID'];
  document: Scalars['Upload'];
}>;


export type UploadDelegationdocumentMutation = { __typename?: 'Mutation', uploadDelegationdocument?: { __typename?: 'UploadDelegationDocument', success?: string | null } | null };

export type GetDelegationContactDetailsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationContactDetailsQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', id: string, email: string, address: string } | null };

export type GetDelegationMembersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationMembersQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', studentSet: Array<{ __typename?: 'StudentType', submitted: boolean, code: string, givenName: string, lastName: string, photo: string }>, leaderSet: Array<{ __typename?: 'LeaderType', type: OlyRegisterLeaderTypeChoices, submitted: boolean, code: string, givenName: string, lastName: string, photo: string }> } | null };

export type GetDelegationTransactionsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDelegationTransactionsQuery = { __typename?: 'Query', delegation?: { __typename?: 'DelegationType', transactionSet: Array<{ __typename?: 'TransactionType', date: any, description: string, amount: any }> } | null };

export type GetStudentQueryVariables = Exact<{
  code: Scalars['String'];
}>;


export type GetStudentQuery = { __typename?: 'Query', student?: { __typename?: 'StudentType', id: string, submitted: boolean, code: string, givenName: string, lastName: string, certificateName: string, gender?: OlyRegisterStudentGenderChoices | null, birthday?: any | null, email: string, emailAuthorization: boolean, mobileNumber: string, schoolName: string, schoolAddress: string, tshirtSize?: OlyRegisterStudentTshirtSizeChoices | null, photo: string, photographyConsent: string, codeOfConduct: string, medicalIssue: string } | null };

export type GetLeaderQueryVariables = Exact<{
  code: Scalars['String'];
}>;


export type GetLeaderQuery = { __typename?: 'Query', leader?: { __typename?: 'LeaderType', id: string, submitted: boolean, code: string, givenName: string, lastName: string, namePrefix: string, gender?: OlyRegisterLeaderGenderChoices | null, birthday?: any | null, email: string, emailAuthorization: boolean, mobileNumber: string, residenceCountry: string, residenceAddress: string, tshirtSize?: OlyRegisterLeaderTshirtSizeChoices | null, photo: string, photographyConsent: string, codeOfConduct: string, medicalIssue: string } | null };

export type GetDocumentsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDocumentsQuery = { __typename?: 'Query', downloads?: Array<{ __typename?: 'DownloadType', id: string, name: string, type: string, desciption: string, icon: string, document: string }> | null };

export type GetDocumentTypesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDocumentTypesQuery = { __typename?: 'Query', documentTypes?: Array<{ __typename?: 'DelegationDocumentTypeType', id: string, order: number, name: string, desciption: string, provider: OlyRegisterDelegationDocumentTypeProviderChoices, mandatory: boolean, multiple: boolean, delegationdocumentSet: Array<{ __typename?: 'DelegationDocumentType', id: string, timestamp: any, document: string }> }> | null };

export type UserQueryVariables = Exact<{ [key: string]: never; }>;


export type UserQuery = { __typename?: 'Query', me?: { __typename?: 'UserType', username: string, delegationSet: Array<{ __typename?: 'DelegationType', name: string, country: string }> } | null };


export const LoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"login"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"tokenAuth"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"username"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"token"}}]}}]}}]} as unknown as DocumentNode<LoginMutation, LoginMutationVariables>;
export const RefreshTokenDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"refreshToken"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"refreshToken"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"token"}}]}}]}}]} as unknown as DocumentNode<RefreshTokenMutation, RefreshTokenMutationVariables>;
export const ForgotPasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"forgotPassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"email"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"forgotPassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"email"},"value":{"kind":"Variable","name":{"kind":"Name","value":"email"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<ForgotPasswordMutation, ForgotPasswordMutationVariables>;
export const UpdateUserPasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateUserPassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"newPassword1"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"newPassword2"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updatePassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"input"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"newPassword1"},"value":{"kind":"Variable","name":{"kind":"Name","value":"newPassword1"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"newPassword2"},"value":{"kind":"Variable","name":{"kind":"Name","value":"newPassword2"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"errors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"field"}},{"kind":"Field","name":{"kind":"Name","value":"messages"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateUserPasswordMutation, UpdateUserPasswordMutationVariables>;
export const UpdateContactDetailsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateContactDetails"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"input"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateDelegationContactMutationInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateContactDetails"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"input"},"value":{"kind":"Variable","name":{"kind":"Name","value":"input"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"delegation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"address"}}]}},{"kind":"Field","name":{"kind":"Name","value":"errors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"field"}},{"kind":"Field","name":{"kind":"Name","value":"messages"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateContactDetailsMutation, UpdateContactDetailsMutationVariables>;
export const CreateStudentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createStudent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createStudent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"student"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}}]}}]}}]}}]} as unknown as DocumentNode<CreateStudentMutation, CreateStudentMutationVariables>;
export const RemoveStudentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeStudent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeStudent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<RemoveStudentMutation, RemoveStudentMutationVariables>;
export const UpdateStudentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateStudent"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"input"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateStudentMutationInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateStudent"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"input"},"value":{"kind":"Variable","name":{"kind":"Name","value":"input"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"student"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"certificateName"}}]}},{"kind":"Field","name":{"kind":"Name","value":"errors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"field"}},{"kind":"Field","name":{"kind":"Name","value":"messages"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateStudentMutation, UpdateStudentMutationVariables>;
export const SubmitStudentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"submitStudent"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"submitStudent"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<SubmitStudentMutation, SubmitStudentMutationVariables>;
export const CreateLeaderDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createLeader"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"type"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"LeaderTypeChoice"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createLeader"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"type"},"value":{"kind":"Variable","name":{"kind":"Name","value":"type"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"leader"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}}]}}]}}]}}]} as unknown as DocumentNode<CreateLeaderMutation, CreateLeaderMutationVariables>;
export const RemoveLeaderDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeLeader"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"type"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"LeaderTypeChoice"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeLeader"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"type"},"value":{"kind":"Variable","name":{"kind":"Name","value":"type"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<RemoveLeaderMutation, RemoveLeaderMutationVariables>;
export const UpdateLeaderDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateLeader"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"input"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateLeaderMutationInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateLeader"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"input"},"value":{"kind":"Variable","name":{"kind":"Name","value":"input"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"leader"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}}]}},{"kind":"Field","name":{"kind":"Name","value":"errors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"field"}},{"kind":"Field","name":{"kind":"Name","value":"messages"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateLeaderMutation, UpdateLeaderMutationVariables>;
export const SubmitLeaderDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"submitLeader"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"submitLeader"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<SubmitLeaderMutation, SubmitLeaderMutationVariables>;
export const UploadDelegationdocumentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"uploadDelegationdocument"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"delegationCode"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"documentType"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"document"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Upload"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"uploadDelegationdocument"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"delegationCode"},"value":{"kind":"Variable","name":{"kind":"Name","value":"delegationCode"}}},{"kind":"Argument","name":{"kind":"Name","value":"documentType"},"value":{"kind":"Variable","name":{"kind":"Name","value":"documentType"}}},{"kind":"Argument","name":{"kind":"Name","value":"document"},"value":{"kind":"Variable","name":{"kind":"Name","value":"document"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<UploadDelegationdocumentMutation, UploadDelegationdocumentMutationVariables>;
export const GetDelegationContactDetailsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getDelegationContactDetails"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"delegation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"address"}}]}}]}}]} as unknown as DocumentNode<GetDelegationContactDetailsQuery, GetDelegationContactDetailsQueryVariables>;
export const GetDelegationMembersDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getDelegationMembers"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"delegation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"studentSet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"submitted"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"leaderSet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"submitted"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}}]}}]}}]}}]} as unknown as DocumentNode<GetDelegationMembersQuery, GetDelegationMembersQueryVariables>;
export const GetDelegationTransactionsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getDelegationTransactions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"delegation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"transactionSet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"amount"}}]}}]}}]}}]} as unknown as DocumentNode<GetDelegationTransactionsQuery, GetDelegationTransactionsQueryVariables>;
export const GetStudentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getStudent"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"student"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"submitted"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"certificateName"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"birthday"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"emailAuthorization"}},{"kind":"Field","name":{"kind":"Name","value":"mobileNumber"}},{"kind":"Field","name":{"kind":"Name","value":"schoolName"}},{"kind":"Field","name":{"kind":"Name","value":"schoolAddress"}},{"kind":"Field","name":{"kind":"Name","value":"tshirtSize"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}},{"kind":"Field","name":{"kind":"Name","value":"photographyConsent"}},{"kind":"Field","name":{"kind":"Name","value":"codeOfConduct"}},{"kind":"Field","name":{"kind":"Name","value":"medicalIssue"}}]}}]}}]} as unknown as DocumentNode<GetStudentQuery, GetStudentQueryVariables>;
export const GetLeaderDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getLeader"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"leader"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"submitted"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"namePrefix"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"birthday"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"emailAuthorization"}},{"kind":"Field","name":{"kind":"Name","value":"mobileNumber"}},{"kind":"Field","name":{"kind":"Name","value":"residenceCountry"}},{"kind":"Field","name":{"kind":"Name","value":"residenceAddress"}},{"kind":"Field","name":{"kind":"Name","value":"tshirtSize"}},{"kind":"Field","name":{"kind":"Name","value":"photo"}},{"kind":"Field","name":{"kind":"Name","value":"photographyConsent"}},{"kind":"Field","name":{"kind":"Name","value":"codeOfConduct"}},{"kind":"Field","name":{"kind":"Name","value":"medicalIssue"}}]}}]}}]} as unknown as DocumentNode<GetLeaderQuery, GetLeaderQueryVariables>;
export const GetDocumentsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getDocuments"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"downloads"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"desciption"}},{"kind":"Field","name":{"kind":"Name","value":"icon"}},{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}}]} as unknown as DocumentNode<GetDocumentsQuery, GetDocumentsQueryVariables>;
export const GetDocumentTypesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getDocumentTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"documentTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"desciption"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"mandatory"}},{"kind":"Field","name":{"kind":"Name","value":"multiple"}},{"kind":"Field","name":{"kind":"Name","value":"delegationdocumentSet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"timestamp"}},{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}}]}}]} as unknown as DocumentNode<GetDocumentTypesQuery, GetDocumentTypesQueryVariables>;
export const UserDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"User"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"delegationSet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"country"}}]}}]}}]}}]} as unknown as DocumentNode<UserQuery, UserQueryVariables>;