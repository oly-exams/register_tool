import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import TodoView from "@/views/TodoView.vue";
import DelegationMembersView from "@/views/DelegationMembersView.vue";
import DelegationEditStudentView from "@/views/DelegationEditStudentView.vue";
import DelegationEditLeaderView from "@/views/DelegationEditLeaderView.vue";
import DelegationDocumentsView from "@/views/DelegationDocumentsView.vue";
import DelegationFinancialView from "@/views/DelegationFinancialView.vue";
import DelegationContactView from "@/views/DelegationContactView.vue";
import DocumentsView from "@/views/DocumentsView.vue";
import ForgotPasswordView from "@/views/ForgotPasswordView.vue";
import { useUserStore } from "@/stores/user";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/delegation/contact",
      name: "delegationContact",
      component: DelegationContactView,
    },
    {
      path: "/delegation/members",
      name: "delegationMembers",
      component: DelegationMembersView,
    },
    {
      path: "/delegation/members/student/:code",
      name: "delegationEditStudent",
      component: DelegationEditStudentView,
    },
    {
      path: "/delegation/members/leader/:code",
      name: "delegationEditLeader",
      component: DelegationEditLeaderView,
    },
    {
      path: "/delegation/documents",
      name: "delegationDocuments",
      component: DelegationDocumentsView,
    },
    {
      path: "/delegation/financial",
      name: "delegationFinancial",
      component: DelegationFinancialView,
    },
    {
      path: "/documents",
      name: "documents",
      component: DocumentsView,
    },
    {
      path: "/todo",
      name: "todo",
      component: TodoView,
    },
    {
      path: "/login",
      name: "login",
      component: LoginView,
    },
    {
      path: "/forgot",
      name: "forgot",
      component: ForgotPasswordView,
    },
  ],
});

// Global Route Guards
router.beforeEach((to, from, next) => {
  console.log("router BEFORE EACH");

  const user = useUserStore();

  // Check if the user is logged in
  const isUserLoggedIn = user.isAuthenticated;
  if (
    to.matched.some(
      (record) => record.name != "login" && record.name != "forgot"
    )
  ) {
    if (!isUserLoggedIn) {
      next({
        path: "/login",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
