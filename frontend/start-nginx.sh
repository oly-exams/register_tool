#!/usr/bin/env bash
export EXISTING_VARS=$(printenv | grep APP_ | awk -F= '{print $1}' | sed 's/^/\$/g' | paste -sd,);

echo ""

mkdir -p /tmp/envsubst
cp $JSFOLDER/*.js /tmp/envsubst

for file in /tmp/envsubst/*.js;
do
  cat $file | envsubst $EXISTING_VARS > $JSFOLDER/$(basename $file)
done
nginx -g 'daemon off;'
