import { fileURLToPath, URL } from "url";
import { resolve } from "path";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@app/gql": resolve(__dirname, "./src/gql"),
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
