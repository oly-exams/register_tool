from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.http import Http404
import secrets
import graphene
from graphene_django import DjangoObjectType
from graphene_django.forms.mutation import DjangoModelFormMutation, DjangoFormMutation
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required, staff_member_required
from graphql import GraphQLError
from oly_register import models, forms

from graphene_django.types import ErrorType
from graphene_django.constants import MUTATION_ERRORS_FLAG

def require_user(info):
    user = info.context.user
    if not user.is_authenticated:
        raise GraphQLError("Authentication credentials were not provided")


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        fields = ('username', 'delegation_set')

class DelegationType(DjangoObjectType):
    class Meta:
        model = models.Delegation

class StudentType(DjangoObjectType):
    photo = graphene.NonNull(graphene.String)
    photography_consent = graphene.NonNull(graphene.String)
    code_of_conduct = graphene.NonNull(graphene.String)

    class Meta:
        model = models.Student
    
    def resolve_photo(self, args):
        return self.photo and self.photo.url

    def resolve_photography_consent(self, args):
        return self.photography_consent and self.photography_consent.url

    def resolve_code_of_conduct(self, args):
        return self.code_of_conduct and self.code_of_conduct.url

class LeaderType(DjangoObjectType):
    photo = graphene.NonNull(graphene.String)
    photography_consent = graphene.NonNull(graphene.String)
    code_of_conduct = graphene.NonNull(graphene.String)

    class Meta:
        model = models.Leader

    def resolve_photo(self, args):
        return self.photo and self.photo.url

    def resolve_photography_consent(self, args):
        return self.photography_consent and self.photography_consent.url

    def resolve_code_of_conduct(self, args):
        return self.code_of_conduct and self.code_of_conduct.url


class TransactionType(DjangoObjectType):
    class Meta:
        model = models.Transaction

class DownloadType(DjangoObjectType):
    document = graphene.NonNull(graphene.String)

    def resolve_document(self, args):
        return self.document.url

    class Meta:
        model = models.Download

class DelegationDocumentTypeType(DjangoObjectType):

    # def resolve_delegationdocument_set(self, args):
    #     if self.
    #     return self.delegationdocument_set.all()

    class Meta:
        model = models.DelegationDocumentType
        # exclude = ('delegationdocument_set', )

class DelegationDocumentType(DjangoObjectType):
    document = graphene.NonNull(graphene.String)

    @classmethod
    def get_queryset(cls, queryset, info):
        if info.context.user.is_staff:
            return queryset

        delegation = info.context.user.delegation_set.first()
        return queryset.filter(delegation=delegation)

    def resolve_document(self, args):
        return self.document.url

    class Meta:
        model = models.DelegationDocument


class Query(graphene.ObjectType):
    me = graphene.Field(UserType)
    all_delegations = graphene.List(graphene.NonNull(DelegationType))
    delegation = graphene.Field(DelegationType)
    delegation_admin = graphene.Field(DelegationType, name=graphene.String(required=True))
    student = graphene.Field(StudentType, code=graphene.String(required=True))
    leader = graphene.Field(LeaderType, code=graphene.String(required=True))
    downloads = graphene.List(graphene.NonNull(DownloadType))
    document_types = graphene.List(graphene.NonNull(DelegationDocumentTypeType))


    @login_required
    def resolve_me(root, info):
        if info.context.user.is_anonymous:
            raise Exception('Authentication Failure!')
        return info.context.user

    @staff_member_required
    def resolve_all_delegations(root, info):
        return models.Delegation.objects.all()

    @login_required
    def resolve_delegation(root, info, **input):
        try:
            delegation = info.context.user.delegation_set.first()
            return delegation
        except models.Delegation.DoesNotExist:
            return None

    @staff_member_required
    def resolve_delegation_admin(root, info, name):
        try:
            return models.Delegation.objects.get(name=name)
        except models.Delegation.DoesNotExist:
            return None

    @login_required
    def resolve_student(root, info, code):
        try:
            user = info.context.user
            if user.is_staff:
                return models.Student.objects.get(code=code)
            user_delegation = info.context.user.delegation_set.first()
            return models.Student.objects.get(code=code, delegation=user_delegation)
        except models.Student.DoesNotExist:
            return None

    @login_required
    def resolve_leader(root, info, code):
        try:
            user = info.context.user
            if user.is_staff:
                return models.Leader.objects.get(code=code)
            user_delegation = info.context.user.delegation_set.first()
            return models.Leader.objects.get(code=code, delegation=user_delegation)
        except models.Leader.DoesNotExist:
            return None

    @login_required
    def resolve_downloads(root, info):
        return models.Download.objects.all()

    @login_required
    def resolve_document_types(root, info):
        return models.DelegationDocumentType.objects.all()


class UpdateDelegationContactMutation(DjangoModelFormMutation):

    class Meta:
        form_class = forms.DelegationContactForm

    @classmethod
    def perform_mutate(cls, form, info):
        require_user(info)
        user = info.context.user
        if user.is_staff or user.delegation_set.filter(id=form.instance.id).exists():
            return super().perform_mutate(form, info)
        
        raise GraphQLError("User is not authorized to operate on this delegation")


class UpdatePassword(DjangoFormMutation):

    class Meta:
        form_class = forms.SetPasswordFormOptionalUser

    @classmethod
    def get_form_kwargs(cls, root, info, **input):
        kwargs = super(cls, cls).get_form_kwargs(root, info, **input)
        require_user(info)
        kwargs["user"] = info.context.user
        return kwargs


class ForgotPassword(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)

    success = graphene.Field(graphene.String)

    @classmethod
    def mutate(cls, root, info, email):
        delegation = models.Delegation.objects.get(email=email)
        if delegation is None:
            raise Http404("Delegation matching this email does not exist")
            return ForgotPassword(success="error")
        
        user = delegation.delegation_user
        new_password = secrets.token_urlsafe(16)
        user.set_password(new_password)
        user.save()
        email_body=f"""Hello,

A password reset for your delegation has been requested.

Your new credentials are:
Username: {delegation.name}
Password: {new_password}

Please contact the organisers, if your login problems persist.

Sincerly,
OlyExams Team
        """
        try:
            send_mail(
                '[IPhO Registration] Password reset',
                email_body,
                None,
                [delegation.email],
            )
        except:
            raise
            return ForgotPassword(success="error while sending the reset email")

        return ForgotPassword(success="ok")


class UpdateStudentMutation(DjangoModelFormMutation):
    class Input:
        photo = Upload()
        photography_consent = Upload()
        code_of_conduct = Upload()

    class Meta:
        form_class = forms.StudentForm
        exclude_fields = ('photo', 'photography_consent', 'code_of_conduct')

    @classmethod
    def get_form_kwargs(cls, root, info, **input):
        kwargs = super(cls, cls).get_form_kwargs(root, info, **input)

        for field_name in ('photo', 'photography_consent', 'code_of_conduct'):
            if field_name in kwargs['data'] and kwargs['data'][field_name]:
                kwargs.setdefault('files', {})[field_name] = kwargs['data'].pop(field_name)

        return kwargs

    @classmethod
    def perform_mutate(cls, form, info):
        require_user(info)
        user = info.context.user
        if user.is_staff or user.delegation_set.filter(id=form.instance.delegation.id).exists():
            return super().perform_mutate(form, info)
        
        if form.instance.submitted:
            raise GraphQLError("The student is already submitted and cannot be edited. Please contact the organisers for changes.")
        
        raise GraphQLError("User is not authorized to operate on this delegation")


class CreateStudent(graphene.Mutation):
    # class Arguments:
    #     delegation = graphene.String(required=True)

    student = graphene.Field(StudentType)

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        current_count = models.Student.objects.filter(delegation=delegation).count()
        if current_count >= 5:
            raise GraphQLError("The delegation reached the maximum number of students.")
        code = f'{delegation.name}-S-{current_count+1}'
        student = models.Student(code=code, delegation=delegation)
        student.save()
        return CreateStudent(student=student)


class RemoveStudent(graphene.Mutation):
    # class Arguments:
    #     delegation = graphene.String(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        current_count = models.Student.objects.filter(delegation=delegation).count()
        if current_count == 0:
            raise GraphQLError("The delegation has no student to remove.")
        student = models.Student.objects.filter(delegation=delegation).order_by("code").last()
        if student is None:
            return RemoveStudent(success="failed")

        if student.submitted:
            raise GraphQLError("The student is already submitted and cannot be removed. Please contact the organisers for changes.")

        student.delete()
        return RemoveStudent(success="ok")


class SubmitStudent(graphene.Mutation):
    class Arguments:
        code = graphene.String(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        student = models.Student.objects.get(code=input["code"], delegation=delegation)
        if student is None:
            return SubmitStudent(success="failed")

        student.submitted = True
        student.save()
        return SubmitStudent(success="ok")


class UpdateLeaderMutation(DjangoModelFormMutation):
    class Input:
        photo = Upload()
        photography_consent = Upload()
        code_of_conduct = Upload()

    class Meta:
        form_class = forms.LeaderForm
        exclude_fields = ('photo', 'photography_consent', 'code_of_conduct')

    @classmethod
    def get_form_kwargs(cls, root, info, **input):
        kwargs = super(cls, cls).get_form_kwargs(root, info, **input)

        for field_name in ('photo', 'photography_consent', 'code_of_conduct'):
            if field_name in kwargs['data'] and kwargs['data'][field_name]:
                kwargs.setdefault('files', {})[field_name] = kwargs['data'].pop(field_name)

        return kwargs

    @classmethod
    def perform_mutate(cls, form, info):
        require_user(info)
        user = info.context.user
        if user.is_staff or user.delegation_set.filter(id=form.instance.delegation.id).exists():
            return super().perform_mutate(form, info)

        if form.instance.submitted:
            raise GraphQLError("The leader is already submitted and cannot be edited. Please contact the organisers for changes.")

        raise GraphQLError("User is not authorized to operate on this delegation")


def leaderTypeDescription(v):
    v = str(v).replace('LeaderTypeChoice.', '')
    for l in models.Leader.TYPE_CHOICES:
        if l[0] == v:
            return l[1]
    return None
def leaderTypeKey(v):
    v = str(v).replace('LeaderTypeChoice.', '')
    for l in models.Leader.TYPE_CHOICES:
        if l[0] == v:
            return l[0]
    return None
LeaderTypeChoice = graphene.Enum(
    "LeaderTypeChoice",
    [l[0] for l in models.Leader.TYPE_CHOICES],
    description=leaderTypeDescription
)

class CreateLeader(graphene.Mutation):
    TYPE_COUNT_MAX = {
        # 'I': 1,
        'L': 2,
    }

    class Arguments:
        type = LeaderTypeChoice(required=True)

    leader = graphene.Field(LeaderType)

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        leader_type = leaderTypeKey(input["type"])
        current_count = models.Leader.objects.filter(delegation=delegation, type=leader_type).count()
        if leader_type in cls.TYPE_COUNT_MAX and current_count >= cls.TYPE_COUNT_MAX[leader_type]:
            raise GraphQLError(f"The delegation reached the maximum number of leader type '{leader_type}'.")
        code = f'{delegation.name}-{leader_type}-{current_count+1}'
        leader = models.Leader(code=code, delegation=delegation, type=leader_type)
        leader.save()
        return CreateLeader(leader=leader)


class RemoveLeader(graphene.Mutation):
    class Arguments:
        type = LeaderTypeChoice(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        leader_type = leaderTypeKey(input["type"])
        current_count = models.Leader.objects.filter(delegation=delegation, type=leader_type).count()
        if current_count == 0:
            raise GraphQLError("The delegation has no leader to remove.")
        leader = models.Leader.objects.filter(delegation=delegation, type=leader_type).order_by("code").last()
        if leader is None:
            return RemoveLeader(success="failed")

        if leader.submitted:
            raise GraphQLError("The leader is already submitted and cannot be removed. Please contact the organisers for changes.")

        leader.delete()
        return RemoveLeader(success="ok")


class SubmitLeader(graphene.Mutation):
    class Arguments:
        code = graphene.String(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        delegation = info.context.user.delegation_set.first()
        leader = models.Leader.objects.get(code=input["code"], delegation=delegation)
        if leader is None:
            return SubmitLeader(success="failed")

        leader.submitted = True
        leader.save()
        return SubmitLeader(success="ok")


class UploadDelegationDocument(graphene.Mutation):
    class Input:
        delegation_code = graphene.String(required=True)
        document_type = graphene.ID(required=True)
        document = Upload(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        user_delegation = info.context.user.delegation_set.first()
        delegation = models.Delegation.objects.get(name=input["delegation_code"])
        if delegation is None:
            raise GraphQLError("Delegation not found")

        if not info.context.user.is_staff and user_delegation != delegation:
            raise GraphQLError("User does not have the permissions to edit this delegation")
        
        doc_type = models.DelegationDocumentType.objects.get(pk=input["document_type"])
        if doc_type is None:
            raise GraphQLError("Document type not found")


        doc = models.DelegationDocument.objects.filter(delegation=delegation, type=doc_type).first()
        if doc is None or doc_type.multiple is True:
            doc = models.DelegationDocument(delegation=delegation, type=doc_type)
        
        doc.document = input["document"]
        doc.save()
        return UploadDelegationDocument(success="ok")


class CreateDelegation(graphene.Mutation):
    class Input:
        code = graphene.String(required=True)
        country = graphene.String(required=True)
        email = graphene.String(required=True)
        password = graphene.String(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        if not info.context.user.is_staff:
            raise GraphQLError("User does not have the permissions to create delegation")
        
        user, created = models.User.objects.get_or_create(username=input["code"])
        if created:
            user.set_password(input["password"])
            user.save()

        d = models.Delegation.objects.get_or_create(
            name=input["code"],
            defaults={
                "country": input["country"],
                "email": input["email"],
                "delegation_user": user,
            }
        )

        return CreateDelegation(success="ok")


class AddTransaction(graphene.Mutation):

    class Input:
        code = graphene.String(required=True)
        date = graphene.DateTime(required=True)
        description = graphene.String(required=True)
        amount = graphene.Decimal(required=True)

    success = graphene.String()

    @classmethod
    def mutate(cls, root, info, **input):
        if not info.context.user.is_staff:
            raise GraphQLError("User does not have the permissions to add transaction")

        delegation = models.Delegation.objects.get(name=input["code"])
        if delegation is None:
            raise GraphQLError("Delegation not found")

        t = models.Transaction(
            delegation=delegation,
            date=input["date"],
            description=input["description"],
            amount=input["amount"],
        )
        t.save()

        return AddTransaction(success="ok")


class Mutation(graphene.ObjectType):
    update_password = UpdatePassword.Field()
    forgot_password = ForgotPassword.Field()
    update_contact_details = UpdateDelegationContactMutation.Field()
    create_student = CreateStudent.Field()
    remove_student = RemoveStudent.Field()
    update_student = UpdateStudentMutation.Field()
    submit_student = SubmitStudent.Field()
    create_leader = CreateLeader.Field()
    remove_leader = RemoveLeader.Field()
    update_leader = UpdateLeaderMutation.Field()
    submit_leader = SubmitLeader.Field()
    upload_delegationdocument = UploadDelegationDocument.Field()
    create_delegation = CreateDelegation.Field()
    add_transaction = AddTransaction.Field()



schema = graphene.Schema(query=Query, mutation=Mutation)
