from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import SetPasswordForm
from .models import Delegation, Student, Leader


class SetPasswordFormOptionalUser(SetPasswordForm):
    # Graphene form is initiazing the form to obtain the fields
    # to avoid init issues, we pass a dummy user argument.
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        super().__init__(user=user, *args, **kwargs)


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        exclude = ('code', 'delegation', 'submitted')


class LeaderForm(forms.ModelForm):

    class Meta:
        model = Leader
        exclude = ('type', 'code', 'delegation', 'submitted')


class DelegationContactForm(forms.ModelForm):

    class Meta:
        model = Delegation
        fields = ('email', 'address')
