from django.apps import AppConfig


class OlyRegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oly_register'
