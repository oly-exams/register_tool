from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.deconstruct import deconstructible

# Create your models here.

class Delegation(models.Model):
    name = models.CharField(unique=True, max_length=24)
    country = models.CharField(unique=True, max_length=100)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=400, blank=True)
    delegation_user = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)

    class Meta(object):
        ordering = ['name']

    def natural_key(self):
        return (self.name, )

    def __str__(self):
        return u'{} ({})'.format(self.country, self.name)


@deconstructible
class PersonCodeUploadDicrectory:

    def __init__(self, prefix):
        self.prefix = prefix

    def __call__(self, instance, filename):
        return f'{self.prefix}/{instance.code}/{filename}'


class Student(models.Model):
    GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'), ('O', 'Other'))
    TSHIRT_CHOICES = (('S', 'Small'), ('M', 'Medium'), ('L', 'Large'), ('XL', 'X-Large'), )

    code = models.CharField(max_length=10, unique=True)
    submitted = models.BooleanField(default=False)

    given_name = models.CharField(max_length=200, blank=True)
    last_name = models.CharField(max_length=200, blank=True)
    certificate_name = models.CharField(max_length=200, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    mobile_number = models.CharField(max_length=24, blank=True)
    email = models.EmailField(blank=True)
    email_authorization = models.BooleanField(default=False)
    school_name = models.CharField(max_length=300, blank=True)
    school_address = models.CharField(max_length=300, blank=True)
    tshirt_size = models.CharField(max_length=4, choices=TSHIRT_CHOICES, blank=True, null=True)
    photo = models.FileField(upload_to=PersonCodeUploadDicrectory('students/photo'), default='default_person.png')
    photography_consent = models.FileField(upload_to=PersonCodeUploadDicrectory('students/photography_consent'), blank=True)
    code_of_conduct = models.FileField(upload_to=PersonCodeUploadDicrectory('students/code_of_conduct'), blank=True)
    medical_issue = models.TextField(blank=True)

    delegation = models.ForeignKey(Delegation, on_delete=models.CASCADE)

    class Meta:
        ordering = ['code']

    def natural_key(self):
        return (self.code, )

    def __str__(self):
        return u'{}'.format(self.code)


class Leader(models.Model):
    TYPE_CHOICES = (('L', 'Team leader'), ('O', 'Observer'), ('I', 'Invigilator'))
    GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'), ('O', 'Other'))
    TSHIRT_CHOICES = (('S', 'Small'), ('M', 'Medium'), ('L', 'Large'), ('XL', 'X-Large'), )

    type = models.CharField(max_length=1, default='T', choices=TYPE_CHOICES)
    code = models.CharField(max_length=10, unique=True)
    submitted = models.BooleanField(default=False)

    given_name = models.CharField(max_length=200, blank=True)
    last_name = models.CharField(max_length=200, blank=True)
    name_prefix = models.CharField(max_length=10, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    mobile_number = models.CharField(max_length=24, blank=True)
    email = models.EmailField(blank=True)
    email_authorization = models.BooleanField(default=False)
    residence_country = models.CharField(max_length=300, blank=True)
    residence_address = models.CharField(max_length=200, blank=True)
    experiment_address = models.CharField(max_length=200, blank=True)
    tshirt_size = models.CharField(max_length=4, choices=TSHIRT_CHOICES, blank=True, null=True)
    photo = models.FileField(upload_to=PersonCodeUploadDicrectory('leaders/photo'), default='default_person.png')
    photography_consent = models.FileField(upload_to=PersonCodeUploadDicrectory('leaders/photography_consent'), blank=True)
    code_of_conduct = models.FileField(upload_to=PersonCodeUploadDicrectory('leaders/code_of_conduct'), blank=True)
    medical_issue = models.TextField(blank=True)

    delegation = models.ForeignKey(Delegation, on_delete=models.CASCADE)

    class Meta:
        ordering = ['code']

    def natural_key(self):
        return (self.code, )

    def __str__(self):
        return u'{}'.format(self.code)


class Transaction(models.Model):
    delegation = models.ForeignKey(Delegation, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    date = models.DateField()

    class Meta:
        ordering = ['date']


class Download(models.Model):
    TYPE_CHOICES = (('M', 'Mandatory'), ('O', 'Optional'))

    name = models.CharField(max_length=200)
    desciption = models.TextField()
    type = models.CharField(max_length=1, )
    icon = models.CharField(max_length=32)
    document = models.FileField(upload_to='downloads/')

    def __str__(self):
        return f'{self.name} [{self.type}]'


class DelegationDocumentType(models.Model):
    PROVIDER_CHOICES = (('D', 'Delegation'), ('O', 'Organizer'))

    order = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(max_length=200)
    desciption = models.TextField(blank=True)
    provider = models.CharField(max_length=4, choices=PROVIDER_CHOICES)
    mandatory = models.BooleanField(default=False)
    multiple = models.BooleanField(default=False)

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return f'{self.name} [{self.provider}]'


@deconstructible
class DelegationUploadDicrectory:

    def __init__(self, prefix):
        self.prefix = prefix

    def __call__(self, instance, filename):
        return f'{self.prefix}/{instance.delegation.name}/{filename}'
    
class DelegationDocument(models.Model):

    delegation = models.ForeignKey(Delegation, on_delete=models.CASCADE)
    type = models.ForeignKey(DelegationDocumentType, on_delete=models.CASCADE)
    timestamp = models.DateField(auto_now=True)
    document = models.FileField(upload_to=DelegationUploadDicrectory('delegation_documents'))

    def __str__(self):
        return f'{self.id} - {self.delegation} [{self.type}]'
