# Generated by Django 4.0.3 on 2022-04-21 05:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oly_register', '0007_alter_leader_email_authorization_alter_leader_gender_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leader',
            name='email_authorization',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='student',
            name='email_authorization',
            field=models.BooleanField(default=False),
        ),
    ]
