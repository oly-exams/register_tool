from django.contrib import admin

from oly_register.models import Delegation, Download, Student, Leader, Transaction, DelegationDocumentType, DelegationDocument

# Register your models here.

@admin.register(Delegation)
class DelegationAdmin(admin.ModelAdmin):
    model = Delegation
    list_display = ('name', 'country')
    search_fields = ['name', 'country']

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    model = Student
    list_filter = ('delegation', 'submitted')
    list_display = ('code', 'given_name', 'last_name')
    search_fields = ['given_name', 'last_name']

@admin.register(Leader)
class LeaderAdmin(admin.ModelAdmin):
    model = Leader
    list_filter = ('delegation', 'type', 'submitted')
    list_display = ('code', 'given_name', 'last_name')
    search_fields = ['given_name', 'last_name']

@admin.register(DelegationDocumentType)
class DelegationDocumentTypeAdmin(admin.ModelAdmin):
    model = DelegationDocumentType
    list_display = ('order', 'name', 'provider', 'mandatory', 'multiple')
    list_display_links = ('name', )
    list_filter = ('provider', )

@admin.register(DelegationDocument)
class DelegationDocumentAdmin(admin.ModelAdmin):
    model = DelegationDocument
    list_display = ('type', 'delegation', 'timestamp')
    list_display_links = ('type', )
    list_filter = ('type', 'delegation', )

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    model = Transaction
    date_hierarchy = 'date'
    list_display = ('date', 'description', 'amount')
    list_filter = ('delegation', )
    search_fields = ['description']

@admin.register(Download)
class DownloadAdmin(admin.ModelAdmin):
    model = Download
    list_display = ('name', 'type')
    list_filter = ('type', )
