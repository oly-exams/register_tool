from storages.backends.s3boto3 import S3Boto3Storage
from django.conf import settings


class StaticStorage(S3Boto3Storage):
    # location = "static"
    # default_acl = "public-read"
    file_overwrite = False
    querystring_auth = False
    bucket_name = settings.STATIC_STORAGE_BUCKET_NAME


class MediaStorage(S3Boto3Storage):
    # location = "media"
    # default_acl = "private"
    file_overwrite = False
    querystring_auth = True
    signature_version = 's3v4'
    bucket_name = settings.MEDIA_STORAGE_BUCKET_NAME
    custom_domain = False # will be set in the url() function

    # from https://github.com/jschneier/django-storages/issues/165#issuecomment-810166563
    def url(self, name, parameters=None, expire=None, http_method=None):
        """Replace internal domain with custom domain for signed URLs."""
        url = super().url(name, parameters, expire, http_method)
        if not hasattr(settings, 'AWS_S3_CUSTOM_DOMAIN'):
            return url
        custom_url = url.replace(
            self.endpoint_url,
            f"{self.url_protocol}//{settings.AWS_S3_CUSTOM_DOMAIN}",
        )
        return custom_url
