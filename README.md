# Registration tool

This tool is managing the registration of the delegation for the scientific olympiads.

## Development

The application is composed by a [backend](./backend) and a [frontend](./component) component. When developing, both components can be launched locally in development mode.

### Backend

This is a [Django](https://docs.djangoproject.com/en/4.0/) project which exposes the models in the django-admin interface for administrative management, and relies on [Django-Graphene](https://github.com/graphql-python/graphene-django) for the interaction with the frontend.

```console
# switch to backend folder
cd backend

# create a virtualenv
python3 -m venv venv

# activate the virtualenv
. ./venv/bin/activate

# install dependencies
pip install -r requirements.txt

# initialize the database
python manage.py migrate

# launch the development server
python manage.py runserver
```

The admin interface is available at [http://localhost:8000/admin/](http://localhost:8000/admin/), and the interactive GraphiQL [http://127.0.0.1:8000/graphql](http://127.0.0.1:8000/graphql).

### Frontend

This part is written in Typescript and it relies on the [Vue 3](https://vuejs.org/guide/introduction.html) framework.

```console
# switch to frontend folder
cd frontend

# install dependencies
npm install

# launch the development server
npm run dev
```

The frontend is then running at [http://localhost:3000/](http://localhost:3000/).

The frontend code is able to generate the required typing automatically from the graphql schema. When implementing new queries or models, follow this approach:
1. Define new queries in `./src/apollo/queries.ts` and new mutations in `./src/apollo/queries.ts`.
2. Generate the new typing with
    ```console
    npm run codegen:graphql
    ```
3. :warning: The auto-generated code is sometime defining a wrong type, which is breaking the compilation. This should be fixed manually after each codegen.
    ```diff
    $ diff src/gql/gql.ts src/gql/gql.ts.fixed
    3c3
    < import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
    ---
    > import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

    $ diff src/gql/graphql.ts src/gql/graphql.ts.fixed
    2c2
    < import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
    ---
    > import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
    ```

### IDE configuration

It is advised to develop using an IDE with support for Typescript and Vue. The official Vue documentation provides instructions on how to improve the setup https://vuejs.org/guide/typescript/overview.html#ide-support.

## Production usage

This repository contains deployment scripts which bootstrap a Kubernetes [k3s cluster](http://k3s.io) which is running all the dependencies and application components.

## License

This codebase is under MIT license. For individual model usage, please refer to the model licenses found in the original packages.
