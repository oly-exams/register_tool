# Bootstrap repo

How this repo was bootstraped


## Backend – Django

```console
django-admin startproject oly_register  .
```

## Frontend - Vuejs

```console
$ npm init vue@latest

Need to install the following packages:
  create-vue@latest
Ok to proceed? (y) 

Vue.js - The Progressive JavaScript Framework

✔ Project name: … oly-register
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add Cypress for both Unit and End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in /Users/michele/Sites/oly-exams/oly_register/frontend/oly-register...

Done. Now run:

  cd oly-register
  npm install
  npm run lint
  npm run dev
```


Intall Apollo for graphql

````console
npm install --save graphql graphql-tag @apollo/client
```


## Useful docs


- https://lorefnon.me/2021/08/08/Vue-composition-api-and-type-safe-graphql-access/
- Example project https://github.com/maxheckel/dirt-simple-initiative
- VueJS with Auth https://github.com/Zoot01/vue3-apollo-full-stack-auth
