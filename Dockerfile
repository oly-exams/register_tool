# syntax=docker/dockerfile:1.3

FROM node:16-bullseye as node-builder

ENV JQ_VERSION=1.6
RUN wget --no-check-certificate https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -O /tmp/jq-linux64
RUN cp /tmp/jq-linux64 /usr/bin/jq
RUN chmod +x /usr/bin/jq

COPY frontend /src
WORKDIR /src
RUN jq 'to_entries | map_values({ (.key) : ("$" + .key) }) | reduce .[] as $item ({}; . + $item)' ./src/config.json > ./src/config.tmp.json && mv ./src/config.tmp.json ./src/config.json
RUN npm install && npm run build


FROM python:3.10-slim as backend-production


RUN apt-get update && apt-get install -y --no-install-recommends \
        apt-transport-https ca-certificates curl gnupg gnupg2 gnupg1 software-properties-common \
    && \
    echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
    && \
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && \
    apt-get update && apt-get install -y --no-install-recommends \
        postgresql-client-14 \
    && \
    apt-get remove -y \
        apt-transport-https ca-certificates curl gnupg gnupg2 gnupg1 software-properties-common \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /app

COPY backend/requirements.txt /tmp/requirements.txt

RUN pip install --upgrade pip \
    && pip install --no-cache -r /tmp/requirements.txt

COPY backend/ /app
WORKDIR /app

ENV PORT=8001

CMD ["sh", "-c", "daphne -b 0.0.0.0 -p $PORT backend.asgi:application"]


FROM nginx:stable as frontend-production

# ENV API_URL=http://localhost:5000
ENV JSFOLDER=/usr/share/nginx/html/assets

# Create directory to hold the sites and cache.
RUN mkdir -p /etc/nginx/sites-enabled

COPY --from=node-builder /src/dist/ /usr/share/nginx/html
COPY frontend/nginx.conf /etc/nginx/nginx.conf
COPY frontend/start-nginx.sh /usr/bin/start-nginx.sh
RUN chmod +x /usr/bin/start-nginx.sh
ENTRYPOINT [ "start-nginx.sh" ]
